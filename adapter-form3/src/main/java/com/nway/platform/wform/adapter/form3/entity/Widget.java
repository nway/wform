package com.nway.platform.wform.adapter.form3.entity;

import lombok.Data;

import java.util.List;

@Data
public class Widget {

    private String id;

    private String type;

    private String category;

    private String icon;

    private Boolean formItemFlag;

    private Boolean internal;

    private Options options;

    private List<Widget> widgetList;
}
