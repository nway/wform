package com.nway.platform.wform.adapter.form3.entity;

import lombok.Data;

import java.util.List;

@Data
public class Options {

    private String name;

    private boolean hidden;

    private String textContent;

    private String onCreated;

    private String onMounted;

    private String label;

    private String size;

    private String labelAlign;

    private String type;

    private String defaultValue;

    private String placeholder;

    private String columnWidth;

    private String labelWidth;

    private boolean labelHidden;

    private boolean readonly;

    private boolean disabled;

    private boolean clearable;

    private boolean showPassword;

    private boolean required;

    private String validation;

    private String validationHint;

    private List<String> customClass;

    private String labelIconClass;

    private String labelIconPosition;

    private String labelTooltip;

    private String minLength;

    private String maxLength;

    private boolean showWordLimit;

    private String prefixIcon;

    private String suffixIcon;

    private boolean appendButton;

    private boolean appendButtonDisabled;

    private String buttonIcon;

    private String onInput;

    private String onChange;

    private String onFocus;

    private String onBlur;

    private String onValidate;
}
