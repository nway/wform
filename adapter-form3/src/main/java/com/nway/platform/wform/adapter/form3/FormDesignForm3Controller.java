package com.nway.platform.wform.adapter.form3;

import com.nway.platform.wform.adapter.form3.entity.Root;
import com.nway.platform.wform.lib.design.dto.BasePage;
import com.nway.platform.wform.lib.design.dto.PageInfoDto;
import com.nway.platform.wform.lib.design.service.PageService;
import com.nway.platform.wform.web.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("formDesign/form3")
public class FormDesignForm3Controller {

    @Autowired
    private PageService pageService;
    @Autowired
    private VariantForm3Adapter adapter;

    @PostMapping("saveFields")
    public Result<Void> saveFields(@RequestBody Root root, @RequestParam("pageId") String pageId) {
        root.getFormConfig().setId(pageId);
        adapter.designConvert(root);
        return Result.ok();
    }

    @GetMapping("getForm")
    public Result<Root> getForm(String pageId) {
        BasePage basePage = pageService.getById(pageId);
        return Result.ok(adapter.renderConvert(basePage));
    }

    @PostMapping("savePage")
    public Result<Void> savePage(@RequestBody PageInfoDto pageInfoDto) {
        pageService.savePage(pageInfoDto);
        return Result.ok();
    }
}
