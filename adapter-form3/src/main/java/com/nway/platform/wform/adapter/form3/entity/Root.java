package com.nway.platform.wform.adapter.form3.entity;

import lombok.Data;

import java.util.List;

@Data
public class Root {

    private List<Widget> widgetList;

    private FormConfig formConfig;
}
