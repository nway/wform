package com.nway.platform.wform.adapter.form3;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.nway.platform.wform.adapter.form3.entity.FormConfig;
import com.nway.platform.wform.adapter.form3.entity.Options;
import com.nway.platform.wform.adapter.form3.entity.Root;
import com.nway.platform.wform.adapter.form3.entity.Widget;
import com.nway.platform.wform.lib.design.constants.PageTypeConstants;
import com.nway.platform.wform.lib.design.dao.FormFieldAttrDao;
import com.nway.platform.wform.lib.design.dto.BasePage;
import com.nway.platform.wform.lib.design.dto.PageFieldDto;
import com.nway.platform.wform.lib.design.dto.PageFieldGroupDto;
import com.nway.platform.wform.lib.design.dto.PageInfoDto;
import com.nway.platform.wform.lib.design.dto.save.PageFieldSaveDto;
import com.nway.platform.wform.lib.design.entity.FormFieldAttr;
import com.nway.platform.wform.lib.design.service.PageService;
import com.nway.platform.wform.lib.design.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class VariantForm3Adapter {

    @Autowired
    private FormFieldAttrDao attrDao;
    @Autowired
    private PageService pageService;

    private static final Map<String, String> WFORM_TO_FORM3_TYPE_MAP = new HashMap<>();
    private static final Map<String, String> FORM3_TO_WFORM_TYPE_MAP = new HashMap<>();

    static {
        WFORM_TO_FORM3_TYPE_MAP.put("text", "static-text");
        FORM3_TO_WFORM_TYPE_MAP.put("static-text", "text");

        WFORM_TO_FORM3_TYPE_MAP.put("text", "input");
        FORM3_TO_WFORM_TYPE_MAP.put("input", "text");

        WFORM_TO_FORM3_TYPE_MAP.put("text", "textarea");
        FORM3_TO_WFORM_TYPE_MAP.put("textarea", "text");

//        WFORM_TO_FORM3_TYPE_MAP.put("key", "input");
//        FORM3_TO_WFORM_TYPE_MAP.put("input", "key");

        WFORM_TO_FORM3_TYPE_MAP.put("nestedForm", "tab");
        FORM3_TO_WFORM_TYPE_MAP.put("tab", "nestedForm");
    }

    public void designConvert(Root root) {

        List<PageFieldSaveDto> retVal = new ArrayList<>();

        for (Widget widget : root.getWidgetList()) {

            PageFieldSaveDto pageField = new PageFieldSaveDto();

            if (StringUtils.contains(widget.getId(), widget.getType())) {
                widget.setId(null);
            }

            pageField.setId(widget.getId());
            pageField.setFieldId(widget.getId());
            pageField.setName(widget.getOptions().getName());
            pageField.setOldName(pageField.getName());
            pageField.setDisplay(widget.getOptions().getLabel());
            pageField.setValidator(StringUtils.defaultIfBlank(widget.getOptions().getValidation(), "none"));
            pageField.setSize(StringUtils.defaultIfBlank(widget.getOptions().getSize(), "36"));
            pageField.setComponent(getComponent(widget.getType()));

            Map<String, String> options = (Map) JSON.toJSON(widget.getOptions());
            options.put("icon", widget.getIcon());
            options.put("category", widget.getCategory());
            options.put("formItemFlag", Boolean.toString(Boolean.TRUE.equals(widget.getFormItemFlag())));
            options.put("internal", Optional.ofNullable(widget.getInternal()).orElse(Boolean.FALSE).toString());

            pageField.setOptions(Collections.singletonMap("options", JSON.toJSONString(options)));

            if (CollectionUtils.isNotEmpty(widget.getWidgetList())) {

                Root rootTmp = new Root();
                FormConfig formConfig = root.getFormConfig();
                formConfig.setId(widget.getId());
                formConfig.setType(PageTypeConstants.PAGE_TYPE_NESTED);
                formConfig.setRefName(widget.getOptions().getName());

                rootTmp.setFormConfig(formConfig);
                rootTmp.setWidgetList(widget.getWidgetList());

                designConvert(rootTmp);
            }

            retVal.add(pageField);
        }


        FormConfig formConfig = root.getFormConfig();

        PageInfoDto pageInfo = new PageInfoDto();
        pageInfo.setId(formConfig.getId());
        pageInfo.setName(formConfig.getRefName());
        pageInfo.setTitle(formConfig.getRefName());

        pageService.savePage(pageInfo);

        FormFieldAttr fieldAttr = new FormFieldAttr();
        fieldAttr.setFieldId(pageInfo.getId());
        fieldAttr.setAttrName("formConfig");
        fieldAttr.setAttrValue(JSON.toJSONString(formConfig));

        attrDao.delete(Lists.newArrayList(fieldAttr.getFieldId()));
        attrDao.insert(Lists.newArrayList(fieldAttr));

        pageService.saveFormFields(pageInfo.getId(), retVal);

    }

    public Root renderConvert(BasePage page) {

        Root root = new Root();

        List<FormFieldAttr> attrs = attrDao.listByFieldId(Lists.newArrayList(page.getId()));
        Map<String, String> attrMap = attrs.stream().collect(Collectors.toMap(FormFieldAttr::getAttrName, FormFieldAttr::getAttrValue));

        FormConfig formConfig = JSON.parseObject(attrMap.get("formConfig"), FormConfig.class);
        formConfig.setModelName(page.getModuleName());
        formConfig.setRefName(page.getName());

        List<Widget> widgetList = new ArrayList<>();

        for (PageFieldDto pageField : page.getFields()) {

            Widget widget = new Widget();

            JSONObject options1 = JSON.parseObject(pageField.getFieldAttr().get("options"));

            widget.setId(pageField.getId());
            widget.setIcon(options1.getString("icon"));
            widget.setType(getType(pageField.getComponent()));
            widget.setCategory(options1.getString("category"));
            widget.setFormItemFlag(options1.getBoolean("formItemFlag"));

            options1.remove("icon");
            options1.remove("category");
            options1.remove("formItemFlag");
            options1.remove("internal");

            widget.setOptions(JSON.parseObject(options1.toJSONString(), Options.class));

            if (pageField instanceof PageFieldGroupDto) {

                BasePage pageInfo = new BasePage();
                pageInfo.setId(pageField.getId());
                pageInfo.setParentId(page.getId());
                pageInfo.setModuleId(page.getModuleId());
                pageInfo.setModuleId(page.getModuleName());
                pageInfo.setType(PageTypeConstants.PAGE_TYPE_NESTED);
                pageInfo.setTableName(((PageFieldGroupDto) pageField).getTableName());
                pageInfo.setName(pageField.getName());
                pageInfo.setTitle(pageField.getDisplay());
                pageInfo.setFields(((PageFieldGroupDto) pageField).getFields());

                List<Widget> widgetListTmp = renderConvert(pageInfo).getWidgetList();
                widget.setWidgetList(widgetListTmp);
            }

            widgetList.add(widget);
        }

        root.setFormConfig(formConfig);
        root.setWidgetList(widgetList);

        return root;
    }

    private String getComponent(String form3type) {

        return FORM3_TO_WFORM_TYPE_MAP.getOrDefault(form3type, form3type);
    }

    private String getType(String comp) {

        return WFORM_TO_FORM3_TYPE_MAP.getOrDefault(comp, comp);
    }
}
