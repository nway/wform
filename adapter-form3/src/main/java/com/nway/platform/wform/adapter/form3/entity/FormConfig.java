package com.nway.platform.wform.adapter.form3.entity;

import lombok.Data;

import java.util.List;

@Data
public class FormConfig {

    private String id;

    private String type;

    private String modelName;

    private String refName;

    private String rulesName;

    private int labelWidth;

    private String labelPosition;

    private String size;

    private String labelAlign;

    private String cssCode;

    private List<String> customClass;

    private String functions;

    private String layoutType;

    private int jsonVersion;

    private String onFormCreated;

    private String onFormMounted;

    private String onFormDataChange;
}
