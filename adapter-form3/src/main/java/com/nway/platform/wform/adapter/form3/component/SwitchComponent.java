package com.nway.platform.wform.adapter.form3.component;

import com.nway.platform.wform.lib.components.SingleValueComponent;
import com.nway.platform.wform.lib.design.db.datatype.DatabaseDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("switchComponent")
public class SwitchComponent implements SingleValueComponent {

    @Autowired
    private DatabaseDialect databaseDialect;

    @Override
    public Class<?> getJavaType() {
        return Boolean.class;
    }

    @Override
    public Object getValue(Object value) {
        return "1".equals(value);
    }

    @Override
    public String getSqlType(String capacity) {
        return databaseDialect.getBoolean();
    }
}
