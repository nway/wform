/*
 Navicat Premium Data Transfer

 Source Server         : local-root
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : wform

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 20/09/2022 18:15:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cmpt_competition_products
-- ----------------------------
DROP TABLE IF EXISTS `cmpt_competition_products`;
CREATE TABLE `cmpt_competition_products`  (
  `id` varchar(36) NOT NULL,
  `code` varchar(36) NULL DEFAULT NULL,
  `cmpt_prod_name` varchar(100) NULL DEFAULT NULL,
  `cmpt_comp_name` varchar(100) NULL DEFAULT NULL,
  `brand` varchar(100) NULL DEFAULT NULL,
  `prod_name` varchar(100) NULL DEFAULT NULL,
  `spec` int(0) NULL DEFAULT NULL,
  `unit` varchar(100) NULL DEFAULT NULL,
  `pkg_type` varchar(100) NULL DEFAULT NULL,
  `cat` varchar(100) NULL DEFAULT NULL,
  `sub_cat` varchar(100) NULL DEFAULT NULL,
  `matker_date` varchar(100) NULL DEFAULT NULL,
  `market_type` varchar(100) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `dept_code` varchar(100) NULL DEFAULT NULL,
  `modifier` varchar(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ;

-- ----------------------------
-- Records of cmpt_competition_products
-- ----------------------------
INSERT INTO `cmpt_competition_products` VALUES ('02c5504355d54b3f88d60e7e3c8159c8', '标签参数5', '什么东西', '哈哈', '1001', '名1', 1011, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-18 17:14:33', '123', '101010');
INSERT INTO `cmpt_competition_products` VALUES ('6f6237588c9b44c9b7e70eed5254a6c5', '标签参数4', '不清楚', NULL, '1001', '二吧', 1011, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-18 17:14:41', '111', '0101010');

-- ----------------------------
-- Table structure for t_auto_1001
-- ----------------------------
DROP TABLE IF EXISTS `t_auto_1001`;
CREATE TABLE `t_auto_1001`  (
  `page_id` varchar(36) NOT NULL,
  `page_name` varchar(36) NULL DEFAULT NULL,
  `is_manual` varchar(36) NULL DEFAULT NULL,
  `table_name` varchar(100) NULL DEFAULT NULL,
  `module_id` varchar(36) NULL DEFAULT NULL,
  `summary` varchar(100) NULL DEFAULT NULL,
  `page_title` varchar(50) NULL DEFAULT NULL,
  PRIMARY KEY (`page_id`) USING BTREE
) ;

-- ----------------------------
-- Records of t_auto_1001
-- ----------------------------
INSERT INTO `t_auto_1001` VALUES ('02c5504355d54b3f88d60e7e3c8159c8', '测试嵌套表单2', '1', 't_abc', '112121', '吞吞吐吐', '哦哦哦');

-- ----------------------------
-- Table structure for t_auto_1001_page_type
-- ----------------------------
DROP TABLE IF EXISTS `t_auto_1001_page_type`;
CREATE TABLE `t_auto_1001_page_type`  (
  `id` varchar(36) NOT NULL,
  `biz_id` varchar(36) NULL DEFAULT NULL,
  `value` varchar(32) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ;

-- ----------------------------
-- Records of t_auto_1001_page_type
-- ----------------------------
INSERT INTO `t_auto_1001_page_type` VALUES ('1347ceb4438d54791db5e83595b27055', 'd8261e5a763146e1992ce4db6a673766', '1002');
INSERT INTO `t_auto_1001_page_type` VALUES ('19b31c8e828af7772e7111dff5780a37', '9e113546fc734721941eb7bbd6d129c8', '1002');
INSERT INTO `t_auto_1001_page_type` VALUES ('1c00a3df650d9ef11fa930e1667566ee', '76afd2ca3c014f889dddff7f10fb12f9', '1002');
INSERT INTO `t_auto_1001_page_type` VALUES ('304e6efd7fd346d2a14116855b63cd49', '31d0b190f5784a328017e61feaa4df09', '1002');
INSERT INTO `t_auto_1001_page_type` VALUES ('3dfbc85ab2ba4ffb8a4fffeff1ea810d', '2af7eba5872640489ff183728f1b2a24', '1002');
INSERT INTO `t_auto_1001_page_type` VALUES ('49fe344ff3e04215ad59fcda57163bd6', 'fb3dcc3fb0c84e3b92fba5b9e0ba5e02', '1002');
INSERT INTO `t_auto_1001_page_type` VALUES ('7b1ba5d071914bc9a12ac9ae182638ba', '3d79e647c2624384a37918ca75f35e8f', '1002');
INSERT INTO `t_auto_1001_page_type` VALUES ('850db0ae73e87f3cb19edc7976b6f1b6', '3a933ba32f6d4be7940d946e6b32ff2e', '1002');
INSERT INTO `t_auto_1001_page_type` VALUES ('a098382194014b8fbc8703f738b36aba', '21d9265b000c4391867b7a740d0d9056', '1002');
INSERT INTO `t_auto_1001_page_type` VALUES ('e46ac123c4f7d1984a86918addc22abc', '25ac9eda933f4aa6807d6f8173faf459', '1002');
INSERT INTO `t_auto_1001_page_type` VALUES ('e96b1260816449b488067e3b222a4036', '3d4f86207efc4617a9a0e6e5a586e95e', '1002');
INSERT INTO `t_auto_1001_page_type` VALUES ('eca22b13748f4c26b6f1af552f510192', '13e029e46b74496b989c554b667ca3be', '1002');
INSERT INTO `t_auto_1001_page_type` VALUES ('f6206be0865b477385df332dfc46f55e', '6f6237588c9b44c9b7e70eed5254a6c5', '1002');
INSERT INTO `t_auto_1001_page_type` VALUES ('fe7ba693fef54f22b6be240614ae8b64', '02c5504355d54b3f88d60e7e3c8159c8', '1002');

-- ----------------------------
-- Table structure for t_form_field
-- ----------------------------
DROP TABLE IF EXISTS `t_form_field`;
CREATE TABLE `t_form_field`  (
  `id` varchar(36) NOT NULL,
  `owner_id` varchar(36) NULL DEFAULT NULL COMMENT '基础表单页面',
  `name` varchar(64) NULL DEFAULT NULL,
  `display` varchar(64) NULL DEFAULT NULL,
  `size` varchar(8) NULL DEFAULT '0',
  `component` varchar(63) NULL DEFAULT NULL,
  `status` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ;

-- ----------------------------
-- Records of t_form_field
-- ----------------------------
INSERT INTO `t_form_field` VALUES ('10', '1001', 'page_name', '名称', '36', 'text', 1, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_field` VALUES ('1005', '1002', 'nest_test', '嵌套测试', '0', 'nestedForm', 1, '2021-09-09 14:54:23', '2021-09-09 14:54:30', b'0');
INSERT INTO `t_form_field` VALUES ('11', '1001', 'page_type', '页面类型', '100', 'checkbox', 1, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_field` VALUES ('12', '1001', 'is_manual', '手动管理', '36', 'singleSelect', 1, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_field` VALUES ('13', '1001', 'table_name', '对应表名', '100', 'text', 1, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_field` VALUES ('14', '1001', 'module_id', '所属模块', '36', 'singleSelect', 1, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_field` VALUES ('15', '1001', 'summary', '简介', '100', 'text', 1, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_field` VALUES ('16', '1002', 'code', '竞品编码', '36', 'text', 1, '2021-09-03 15:57:41', '2021-09-03 15:57:45', b'0');
INSERT INTO `t_form_field` VALUES ('17', '1002', 'cmpt_prod_name', '竞品名', '100', 'text', 1, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_field` VALUES ('18', '1002', 'cmpt_comp_name', '竞品公司', '100', 'text', 1, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_field` VALUES ('19', '1002', 'brand', '品牌', '100', 'singleSelect', 1, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_field` VALUES ('20', '1002', 'prod_name', '产品名称', '100', 'text', 1, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_field` VALUES ('21', '1002', 'spec', '规格', '3', 'integer', 1, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_field` VALUES ('22', '1002', 'unit', '单位', '100', 'singleSelect', 1, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_field` VALUES ('23', '1002', 'pkg_type', 'singleSelect', '100', 'text', 1, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_field` VALUES ('24', '1002', 'cat', '品类', '100', 'singleSelect', 1, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_field` VALUES ('25', '1002', 'sub_cat', '子品类', '100', 'text', 1, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_field` VALUES ('26', '1002', 'matker_date', '上市时间', '100', 'text', 1, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_field` VALUES ('27', '1002', 'market_type', '上市类型', '100', 'singleSelect', 1, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_field` VALUES ('28', '1002', 'update_time', '更新时间', '100', 'localDateTime', 1, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_field` VALUES ('29', '1002', 'dept_code', '部门编码', '100', 'text', 1, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_field` VALUES ('30', '1002', 'modifier', '人员', '100', 'text', 1, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_field` VALUES ('31', '1002', 'id', '主键', '36', 'key', 1, '2021-09-06 13:04:54', '2021-09-06 12:54:12', b'0');
INSERT INTO `t_form_field` VALUES ('32', '1005', 'page_id', '主键', '36', 'key', 1, '2022-06-15 15:08:00', '2022-06-15 15:07:48', b'0');
INSERT INTO `t_form_field` VALUES ('33', '1005', 'page_name', '名称', '36', 'text', 1, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_field` VALUES ('570310034197651458', '1006', 'input23208', '主键吧', '10', 'key', NULL, '2022-09-20 18:02:02', NULL, b'0');
INSERT INTO `t_form_field` VALUES ('603610370060906498', '1006', 'name2', '姓名3', '10', 'text', NULL, '2022-09-20 18:02:02', NULL, b'0');
INSERT INTO `t_form_field` VALUES ('603610370069295105', '1006', 'productDate', '生产日期', '10', 'date', NULL, '2022-09-20 18:02:02', NULL, b'0');
INSERT INTO `t_form_field` VALUES ('8', '1001', 'page_id', '主键', '36', 'key', 1, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_field` VALUES ('9', '1001', 'page_title', '标题', '50', 'text', 1, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');

-- ----------------------------
-- Table structure for t_form_field_attr
-- ----------------------------
DROP TABLE IF EXISTS `t_form_field_attr`;
CREATE TABLE `t_form_field_attr`  (
  `id` varchar(36) NOT NULL,
  `field_id` varchar(36) NULL DEFAULT NULL,
  `attr_name` varchar(64) NULL DEFAULT NULL,
  `attr_value` varchar(5000) NULL DEFAULT NULL,
  `is_deleted` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ;

-- ----------------------------
-- Records of t_form_field_attr
-- ----------------------------
INSERT INTO `t_form_field_attr` VALUES ('1', '19', 'dic', 'pageType', b'0');
INSERT INTO `t_form_field_attr` VALUES ('2', '22', 'sql', 'select name key,display value from t_form_field', b'0');
INSERT INTO `t_form_field_attr` VALUES ('3', '1005', 'nested_page_name', 'firstPage', b'0');
INSERT INTO `t_form_field_attr` VALUES ('4', '1005', 'tableName', 't_auto_1001', b'0');
INSERT INTO `t_form_field_attr` VALUES ('570339669547503622', '570339468732616706', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"text-field\",\"labelWidth\":\"\",\"type\":\"text\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"custom-search\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"input\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"input66902\",\"customClass\":[],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('570339899315671046', '570339321265082370', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"text-field\",\"labelWidth\":\"\",\"type\":\"text\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"custom-search\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"input\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"input66902\",\"customClass\":[],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('570349255839035397', '570341109586309122', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"text-field\",\"labelWidth\":\"\",\"type\":\"text\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"custom-search\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"input\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"input23797\",\"customClass\":[],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('570349255839035398', '570349255826452481', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"suffixIcon\":\"\",\"icon\":\"text-field\",\"type\":\"text\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"custom-search\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"label\":\"input\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"input96846\",\"customClass\":[],\"showPassword\":false,\"prefixIcon\":\"\",\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603554019058208774', 'daterange45243', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"date-range-field\",\"type\":\"daterange\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"生产日期\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate\",\"customClass\":[\"\"],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603559832753598469', '603559549407391745', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"date-field\",\"type\":\"date\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"生产日期\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate\",\"customClass\":[\"\"],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603560126178717702', '603560108155793410', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"date-field\",\"type\":\"date\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"生产日期\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate\",\"customClass\":[\"\"],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603560912832991238', '603560894004760577', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"date-field\",\"type\":\"date\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"生产日期\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate\",\"customClass\":[\"\"],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603561194719580165', '603561194690220033', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"date-field\",\"type\":\"date\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"生产日期\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate\",\"customClass\":[\"\"],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603586810563063813', '603586743542280193', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"date-field\",\"type\":\"date\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"date\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate2\",\"customClass\":[],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603588339890188293', '603587767946506241', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"date-field\",\"type\":\"date\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"date\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate2\",\"customClass\":[],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603588513068806149', '603588510296371202', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"date-field\",\"type\":\"date\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"date\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate2\",\"customClass\":[],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603607094619860998', '603607087120445442', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"date-field\",\"type\":\"date\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"date\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate2\",\"customClass\":[],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603607948957642757', '603607948932476930', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"date-field\",\"type\":\"date\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"date\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate2\",\"customClass\":[],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603608288838873089', '603608288654323713', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"text-field\",\"labelWidth\":\"\",\"type\":\"text\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"custom-search\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"主键吧\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"input23208\",\"customClass\":[],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603608288838873090', '603608288666906626', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"false\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"switch-field\",\"labelWidth\":\"\",\"type\":\"\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":false,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"什么开关呢\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"switch\",\"customClass\":[\"\"],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603608288838873091', '603608288683683842', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"textarea-field\",\"labelWidth\":\"\",\"type\":\"\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":false,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"测试文本框\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"textareaTest\",\"customClass\":[\"\"],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603608288838873092', '603608288696266753', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"text-field\",\"labelWidth\":\"\",\"type\":\"text\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"custom-search\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"又加了一个\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"more\",\"customClass\":[\"\"],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603608288838873093', '603608288708849666', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"date-field\",\"type\":\"date\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"date\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate2\",\"customClass\":[],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603608549313540102', '603608549233848322', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"date-field\",\"type\":\"date\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"date\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate2\",\"customClass\":[],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603608625847005186', '570310034201845762', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"false\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"switch-field\",\"labelWidth\":\"\",\"type\":\"\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":false,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"什么开关呢\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"switch\",\"customClass\":[\"\"],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603608625847005187', '570310034206040065', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"textarea-field\",\"labelWidth\":\"\",\"type\":\"\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":false,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"测试文本框\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"textareaTest\",\"customClass\":[\"\"],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603608625847005188', '570331060025511938', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"text-field\",\"labelWidth\":\"\",\"type\":\"text\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"custom-search\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"又加了一个\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"more\",\"customClass\":[\"\"],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603608625847005189', '603608625805062146', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"date-field\",\"type\":\"date\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"date\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate2\",\"customClass\":[],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603609471057981442', '603609470978289666', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"suffixIcon\":\"\",\"icon\":\"text-field\",\"type\":\"text\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"custom-search\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"label\":\"姓名\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"name\",\"customClass\":[],\"showPassword\":false,\"prefixIcon\":\"\",\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603609471057981443', '603609470990872578', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"time-field\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"生产日期\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate\",\"customClass\":[\"\"],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603609992456105986', '603609992422551554', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"suffixIcon\":\"\",\"icon\":\"text-field\",\"type\":\"text\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"custom-search\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"label\":\"姓名\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"name\",\"customClass\":[],\"showPassword\":false,\"prefixIcon\":\"\",\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603609992456105987', '603609992426745857', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"date-field\",\"type\":\"date\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"生产日期\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate\",\"customClass\":[\"\"],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603610864485462019', '603610864447713281', 'options', '{\"internal\":\"false\",\"hidden\":false,\"icon\":\"date-field\",\"type\":\"date\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"readonly\":false,\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"label\":\"生产日期\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate\",\"customClass\":[\"\"],\"showPassword\":false,\"onCreated\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603611177380540419', '603610864443518977', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"text-field\",\"labelWidth\":\"\",\"type\":\"text\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"custom-search\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"姓名\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"name\",\"customClass\":[],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603612033547038726', '603611735642402817', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"textarea-field\",\"labelWidth\":\"\",\"type\":\"\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":false,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"长文本3\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"longText3\",\"customClass\":[\"\"],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603617502181257221', '603617481763385346', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"textarea-field\",\"labelWidth\":\"\",\"type\":\"\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":false,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"长文本\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"longText12\",\"customClass\":[\"\"],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603618827442909189', '603618810476949505', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"textarea-field\",\"labelWidth\":\"\",\"type\":\"\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":false,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"长文本\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"longText12\",\"customClass\":[\"\"],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603618827442909190', '603618810481143809', 'options', '{\"clearable\":false,\"internal\":\"false\",\"hidden\":false,\"icon\":\"tab\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"false\",\"labelHidden\":false,\"readonly\":false,\"showWordLimit\":false,\"name\":\"tab101052\",\"customClass\":[],\"showPassword\":false,\"appendButton\":false,\"disabled\":false,\"category\":\"container\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603619166179041285', '603619166124515330', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"textarea-field\",\"labelWidth\":\"\",\"type\":\"\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":false,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"长文本\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"longText12\",\"customClass\":[\"\"],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603619166179041286', '603619166132903937', 'options', '{\"clearable\":false,\"internal\":\"false\",\"hidden\":false,\"icon\":\"tab\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"false\",\"labelHidden\":false,\"readonly\":false,\"showWordLimit\":false,\"name\":\"tab101052\",\"customClass\":[],\"showPassword\":false,\"appendButton\":false,\"disabled\":false,\"category\":\"container\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603675035675697153', '1006', 'formConfig', '{\"cssCode\":\"\",\"customClass\":[\"\"],\"functions\":\"\",\"id\":\"1006\",\"jsonVersion\":3,\"labelAlign\":\"label-left-align\",\"labelPosition\":\"left\",\"labelWidth\":80,\"layoutType\":\"PC\",\"modelName\":\"firstModule\",\"onFormCreated\":\"\",\"onFormDataChange\":\"\",\"onFormMounted\":\"\",\"refName\":\"vForm\",\"rulesName\":\"rules\",\"size\":\"\",\"type\":\"form\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603675035767971842', '570310034197651458', 'options', '{\"internal\":\"false\",\"hidden\":true,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"text-field\",\"labelWidth\":\"\",\"type\":\"text\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"custom-search\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"主键吧\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"input23208\",\"customClass\":[],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603675035767971843', '603610370069295105', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"date-field\",\"labelWidth\":\"\",\"type\":\"date\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"生产日期\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"productDate\",\"customClass\":[\"\"],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);
INSERT INTO `t_form_field_attr` VALUES ('603675035767971844', '603610370060906498', 'options', '{\"internal\":\"false\",\"hidden\":false,\"defaultValue\":\"\",\"minLength\":\"\",\"suffixIcon\":\"\",\"icon\":\"text-field\",\"labelWidth\":\"\",\"type\":\"text\",\"required\":false,\"appendButtonDisabled\":false,\"formItemFlag\":\"true\",\"buttonIcon\":\"custom-search\",\"readonly\":false,\"labelTooltip\":\"\",\"showWordLimit\":false,\"columnWidth\":\"200px\",\"appendButton\":false,\"disabled\":false,\"labelIconClass\":\"\",\"placeholder\":\"\",\"validation\":\"\",\"clearable\":true,\"onMounted\":\"\",\"onChange\":\"\",\"onInput\":\"\",\"textContent\":\"\",\"label\":\"姓名3\",\"labelIconPosition\":\"rear\",\"onFocus\":\"\",\"validationHint\":\"\",\"onBlur\":\"\",\"labelHidden\":false,\"size\":\"\",\"labelAlign\":\"\",\"onValidate\":\"\",\"name\":\"name3\",\"customClass\":[],\"showPassword\":false,\"prefixIcon\":\"\",\"category\":\"\",\"onCreated\":\"\",\"maxLength\":\"\"}', NULL);

-- ----------------------------
-- Table structure for t_form_module
-- ----------------------------
DROP TABLE IF EXISTS `t_form_module`;
CREATE TABLE `t_form_module`  (
  `id` varchar(32) NOT NULL,
  `name` varchar(100) NULL DEFAULT NULL,
  `title` varchar(100) NULL DEFAULT NULL,
  `status` int(0) NULL DEFAULT NULL,
  `summary` varchar(255) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ;

-- ----------------------------
-- Records of t_form_module
-- ----------------------------
INSERT INTO `t_form_module` VALUES ('100101', 'firstModule', '测试模块', NULL, NULL, NULL, '2021-07-20 11:01:52', b'0');
INSERT INTO `t_form_module` VALUES ('100102', 'formDesign', '表单设计', NULL, NULL, NULL, '2021-07-20 11:01:52', b'0');

-- ----------------------------
-- Table structure for t_form_page_field
-- ----------------------------
DROP TABLE IF EXISTS `t_form_page_field`;
CREATE TABLE `t_form_page_field`  (
  `id` varchar(36) NOT NULL,
  `owner_id` varchar(36) NULL DEFAULT NULL,
  `field_id` varchar(36) NULL DEFAULT NULL,
  `validator` varchar(64) NULL DEFAULT 'none' ,
  `is_authority_field` bit(1) DEFAULT null COMMENT '是否为权限字段',
  `is_hidden` bit(1) NULL DEFAULT null COMMENT '是否显隐藏',
  `status` tinyint NULL DEFAULT NULL,
  `field_order` tinyint(0) NULL DEFAULT NULL COMMENT '页/组内顺序',
  `update_time` datetime(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ;

-- ----------------------------
-- Records of t_form_page_field
-- ----------------------------
INSERT INTO `t_form_page_field` VALUES ('10', '1001', '10', 'none', b'0', b'0', 1, 10, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_page_field` VALUES ('1005', '1002', '1005', 'none', b'0', b'0',  1, 10, '2021-09-09 14:54:23', '2021-09-09 14:54:30', b'0');
INSERT INTO `t_form_page_field` VALUES ('11', '1001', '11', 'none',  b'0', b'0', 1, 10, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_page_field` VALUES ('12', '1001', '12', 'none', b'0', b'0',  1, 10, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_page_field` VALUES ('13', '1001', '13', 'none', b'0', b'0',  1, 10, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_page_field` VALUES ('14', '1001', '14', 'none', b'0', b'0',  1, 10, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_page_field` VALUES ('15', '1001', '15', 'none', b'0', b'0',  1, 10, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_page_field` VALUES ('16', '1002', '16', 'none',  b'0', b'0', 1, 2, '2021-09-03 15:57:41', '2021-09-03 15:57:45', b'0');
INSERT INTO `t_form_page_field` VALUES ('17', '1002', '17', 'none', b'0', b'0',  1, 10, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_page_field` VALUES ('18', '1002', '18', 'none', b'0', b'0',  1, 10, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_page_field` VALUES ('19', '1002', '19', 'none', b'0', b'0',  1, 10, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_page_field` VALUES ('20', '1002', '20', 'none', b'0', b'0',  1, 10, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_page_field` VALUES ('21', '1002', '21', 'none', b'0', b'0',  1, 10, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_page_field` VALUES ('22', '1002', '22', 'none', b'0', b'0',  1, 10, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_page_field` VALUES ('23', '1002', '23', 'none',  b'0', b'0', 1, 10, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_page_field` VALUES ('24', '1002', '24', 'none', b'0', b'0',  1, 10, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_page_field` VALUES ('25', '1002', '25', 'none', b'0', b'0',  1, 10, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_page_field` VALUES ('26', '1002', '26', 'none', b'0', b'0',  1, 10, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_page_field` VALUES ('27', '1002', '27', 'none',  b'0', b'0', 1, 10, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_page_field` VALUES ('28', '1002', '28', 'none',  b'0', b'0', 1, 10, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_page_field` VALUES ('29', '1002', '29', 'none', b'0', b'0',  1, 10, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_page_field` VALUES ('30', '1002', '30', 'none',  b'0', b'0', 1, 10, '2021-09-03 15:59:06', '2021-09-03 15:59:09', b'0');
INSERT INTO `t_form_page_field` VALUES ('31', '1002', '31', 'none', b'0', b'0',  1, 1, '2021-09-06 13:04:54', '2021-09-06 12:54:12', b'0');
INSERT INTO `t_form_page_field` VALUES ('32', '1003', '17', 'none',  b'0', b'0', 1, 1, '2021-09-28 13:18:04', '2021-09-28 13:18:07', b'0');
INSERT INTO `t_form_page_field` VALUES ('33', '1003', '21', 'none', b'0', b'0',  1, 2, '2021-09-28 13:18:35', '2021-09-28 13:18:40', b'0');
INSERT INTO `t_form_page_field` VALUES ('34', '1003', '31', 'none',  b'0', b'0', 1, 0, '2021-09-28 13:19:09', '2021-09-28 13:19:12', b'0');
INSERT INTO `t_form_page_field` VALUES ('35', '1003', '18', 'none',  b'0', b'0', 1, 3, '2021-09-28 13:35:01', '2021-09-28 13:35:03', b'0');
INSERT INTO `t_form_page_field` VALUES ('36', '1005', '33', 'none', b'0', b'0',  1, 10, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_page_field` VALUES ('37', '1005', '32', 'none',  b'0', b'0', 1, 1, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_page_field` VALUES ('570310034197651458', '1006', '570310034197651458', 'none', b'0', b'0',  1, 0, '2022-09-20 18:02:02', NULL, b'0');
INSERT INTO `t_form_page_field` VALUES ('603610370060906498', '1006', '603610370060906498', 'none', b'0', b'0',  NULL, 2, '2022-09-20 18:02:02', NULL, b'0');
INSERT INTO `t_form_page_field` VALUES ('603610370069295105', '1006', '603610370069295105', 'none', b'0', b'0',  NULL, 1, '2022-09-20 18:02:02', NULL, b'0');
INSERT INTO `t_form_page_field` VALUES ('8', '1001', '8', 'none', b'0', b'0',  1, 1, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');
INSERT INTO `t_form_page_field` VALUES ('9', '1001', '9', 'none',  b'0', b'0', 1, 10, '2017-12-16 20:42:50', '2017-12-16 20:42:52', b'0');

-- ----------------------------
-- Table structure for t_form_page_info
-- ----------------------------
DROP TABLE IF EXISTS `t_form_page_info`;
CREATE TABLE `t_form_page_info`  (
  `id` varchar(36) NOT NULL,
  `parent_id` varchar(36) NULL DEFAULT NULL COMMENT '上级页面id',
  `module_id` varchar(32) NULL DEFAULT NULL,
  `name` varchar(100) NULL DEFAULT NULL,
  `title` varchar(100) NULL DEFAULT NULL,
  `status` int(0) NULL DEFAULT NULL,
  `type` varchar(10) NULL DEFAULT NULL,
  `gtx_create` bit(1) NULL DEFAULT b'0' COMMENT '新增数据时是否需要全局事务',
  `gtx_update` bit(1) NULL DEFAULT b'0' COMMENT '修改数据时是否需要全局事务',
  `gtx_delete` bit(1) NULL DEFAULT b'0' COMMENT '删除数据时是否需要全局事务',
  `editable` bit(1) NULL DEFAULT b'1' COMMENT '是否可编辑',
  `is_manual` bit(1) NULL DEFAULT b'1' COMMENT '0：根据字典定义自动创建表；1：直接使用现有表',
  `table_name` varchar(30) NULL DEFAULT NULL,
  `summary` varchar(255) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ;

-- ----------------------------
-- Records of t_form_page_info
-- ----------------------------
INSERT INTO `t_form_page_info` VALUES ('1001', NULL, '100101', 'firstPage', '第一个页面', 1, 'form', b'0', b'0', b'0', NULL, b'0', 't_auto_1001', '测试', '2017-12-16 20:14:52', '2017-12-16 20:14:54', b'0');
INSERT INTO `t_form_page_info` VALUES ('1002', NULL, '100101', 'competitionProducts', '竞品产品', 1, 'form', b'1', b'1', b'0', NULL, b'0', 'cmpt_competition_products', '竞品产品基础信息', '2021-09-03 15:38:20', '2021-09-03 15:38:15', b'0');
INSERT INTO `t_form_page_info` VALUES ('1003', '1002', '100101', 'simpleList', '简化信息的列表', 1, 'list', b'0', b'0', b'0', b'0', b'1', 'cmpt_competition_products', '不可编辑', '2021-09-28 13:35:53', '2021-09-28 13:13:34', b'0');
INSERT INTO `t_form_page_info` VALUES ('1005', '1002', '100101', 'nested', '嵌套的', 1, 'nested', b'0', b'0', b'0', b'1', b'1', 't_auto_1001', NULL, NULL, '2022-06-15 14:51:15', b'0');
INSERT INTO `t_form_page_info` VALUES ('1006', NULL, '100101', 'vForm', 'vForm', 0, 'form', b'0', b'0', b'0', b'1', b'0', 't_vform3', '集成测试', '2022-09-20 18:02:02', '2021-09-03 15:38:15', b'0');

-- ----------------------------
-- Table structure for t_form_page_list
-- ----------------------------
DROP TABLE IF EXISTS `t_form_page_list`;
CREATE TABLE `t_form_page_list`  (
  `id` varchar(36) NOT NULL,
  `page_id` varchar(36) NULL DEFAULT NULL,
  `field_id` varchar(36) NULL DEFAULT NULL,
  `search_type` varchar(15) NULL DEFAULT NULL,
  `order_type` varchar(5) NULL DEFAULT NULL,
  `order_num` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ;

-- ----------------------------
-- Records of t_form_page_list
-- ----------------------------
INSERT INTO `t_form_page_list` VALUES ('14', '1003', '20', 'like', NULL, NULL, NULL, '2022-01-25 15:06:30', b'0');
INSERT INTO `t_form_page_list` VALUES ('16', '1003', '30', '', NULL, NULL, NULL, '2022-01-25 15:06:30', b'0');
INSERT INTO `t_form_page_list` VALUES ('17', '1003', '29', '=', 'desc', NULL, NULL, '2022-06-15 14:33:28', NULL);
INSERT INTO `t_form_page_list` VALUES ('4', '1001', '9', 'like', 'desc', NULL, NULL, '2022-01-25 15:06:30', b'0');
INSERT INTO `t_form_page_list` VALUES ('5', '1001', '10', '=', 'asc', NULL, NULL, '2022-01-25 15:06:30', b'0');
INSERT INTO `t_form_page_list` VALUES ('6', '1001', '8', 'in', NULL, NULL, NULL, '2022-01-25 15:06:30', b'0');
INSERT INTO `t_form_page_list` VALUES ('7', '1001', '13', 'right-like', 'desc', NULL, NULL, '2022-01-25 15:06:30', b'0');
INSERT INTO `t_form_page_list` VALUES ('8', '1001', '11', 'in', NULL, NULL, NULL, '2022-01-25 15:06:30', b'0');

-- ----------------------------
-- Table structure for t_vform3
-- ----------------------------
DROP TABLE IF EXISTS `t_vform3`;
CREATE TABLE `t_vform3`  (
  `input23208` varchar(255) NOT NULL,
  `product_date` datetime(0) NULL DEFAULT NULL COMMENT '生产日期',
  `name2` varchar(10) NULL DEFAULT NULL COMMENT '长文本',
  PRIMARY KEY (`input23208`) USING BTREE
) ;

-- ----------------------------
-- Records of t_vform3
-- ----------------------------
INSERT INTO `t_vform3` VALUES ('85a4b8b807be40568bbac3264266dac5', NULL, NULL);

-- ----------------------------
-- Table structure for undo_log
-- ----------------------------
DROP TABLE IF EXISTS `undo_log`;
CREATE TABLE `undo_log`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(0) NOT NULL,
  `xid` varchar(100)  NOT NULL,
  `context` varchar(128)  NOT NULL,
  `rollback_info` longblob NOT NULL,
  `log_status` int(0) NOT NULL,
  `log_created` datetime(0) NOT NULL,
  `log_modified` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `ux_undo_log`(`xid`, `branch_id`) USING BTREE
);

-- ----------------------------
-- Records of undo_log
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
