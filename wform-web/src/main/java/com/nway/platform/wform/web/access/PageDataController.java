package com.nway.platform.wform.web.access;

import com.nway.platform.wform.lib.access.PageDataService;
import com.nway.platform.wform.lib.components.Initializable;
import com.nway.platform.wform.lib.design.dto.BasePage;
import com.nway.platform.wform.lib.design.dto.PageFieldDto;
import com.nway.platform.wform.lib.design.service.PageService;
import com.nway.platform.wform.web.vo.Result;
import com.nway.spring.jdbc.pagination.Page;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("pageData")
public class PageDataController {

    @Autowired
    private PageService pageService;
    @Autowired
    private PageDataService pageDataService;

    @GetMapping("/{pageName}/getInitialData")
    public Result<Map<String, Object>> getInitialData(@PathVariable("pageName") String pageName, HttpServletRequest request) {

        Map<String, Object> retVal = new HashMap<>();
        Map<String, Object> dataModel = new HashMap<>();

        BasePage formPage = pageService.getByName(pageName);
        Map<String, Map<String, String>> fieldMeta = pageService.listFieldMeta(formPage.getId());

        for (PageFieldDto field : formPage.getFields()) {
            if (field.getComponentObj() instanceof Initializable) {
                dataModel.put(field.getName(), ((Initializable) field.getComponentObj()).init(field));
            }
        }

        retVal.put("dataModel", dataModel);
        retVal.put("fieldMeta", fieldMeta);
        retVal.put("page", formPage);

        return Result.ok(retVal);
    }

    @PostMapping("/{pageName}/save")
    public Result<Map<String, Object>> save(@PathVariable("pageName") String pageName, @RequestBody Map<String, Object> pageData) {

        pageDataService.save(pageName, pageData);

        return Result.ok();
    }

    @GetMapping("/{pageName}/details")
    public Result<Map<String, Object>> details(@PathVariable("pageName") String pageName, @RequestParam String bizId) {

        return Result.ok(pageDataService.get(pageName, bizId));
    }

    @GetMapping("/{pageName}/queryPage")
    public Result<Page<Map<String, Object>>> queryPage(@PathVariable("pageName") String pageName, @RequestParam Map<String, Object> pageParam) {

        return Result.ok(pageDataService.queryPage(pageName, pageParam));
    }

    @GetMapping("/{pageName}/export")
    public void export(@PathVariable("pageName") String pageName, @RequestParam Map<String, Object> pageParam, HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String title = pageService.getByName(pageName).getTitle();
        String fileName = URLEncoder.encode(title, "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        pageDataService.export(pageName, pageParam, response.getOutputStream());
    }

}
