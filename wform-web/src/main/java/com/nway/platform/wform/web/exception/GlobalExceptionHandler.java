package com.nway.platform.wform.web.exception;

import com.nway.platform.wform.lib.exception.WFormException;
import com.nway.platform.wform.web.vo.Result;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public Object handleException(Exception ex, HttpServletResponse response) throws IOException {
        log.error(ex.getMessage(), ex);
        return Result.error("系统错误");
    }

    @ExceptionHandler(WFormException.class)
    public Object handleWFormException(WFormException ex, HttpServletResponse response) throws IOException {
        log.error(ex.getMessage(), ex);
        return Result.error(ex.getMessage());
    }

}
