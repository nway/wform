package com.nway.platform.wform.web.design;

import com.nway.platform.wform.lib.design.dto.BasePage;
import com.nway.platform.wform.lib.design.dto.ListPageDto;
import com.nway.platform.wform.lib.design.dto.PageInfoDto;
import com.nway.platform.wform.lib.design.dto.PageNodeDto;
import com.nway.platform.wform.lib.design.dto.save.PageFieldSaveDto;
import com.nway.platform.wform.lib.design.query.PageListQuery;
import com.nway.platform.wform.lib.design.service.PageService;
import com.nway.platform.wform.web.vo.Result;
import com.nway.spring.jdbc.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("formDesign")
public class FormDesignController {

    @Autowired
    private PageService pageService;

    @PostMapping("saveFields")
    public Result<Void> saveFields(@RequestBody List<PageFieldSaveDto> fields, @RequestParam("pageId") String pageId) {
        pageService.saveFormFields(pageId, fields);
        return Result.ok();
    }

    @PostMapping("savePage")
    public Result<Void> savePage(@RequestBody PageInfoDto pageInfoDto) {
        pageService.savePage(pageInfoDto);
        return Result.ok();
    }

    @GetMapping("pageList")
    public Result<Page<ListPageDto>> pageList(PageListQuery query) {
        return Result.ok(pageService.pageList(query));
    }

    @GetMapping("pageFields")
    public Result<BasePage> pageFields(String pageId) {
        return Result.ok(pageService.getById(pageId));
    }

    @GetMapping("publishPage")
    public Result<String> publishPage(@RequestParam String pageId) {

        pageService.publishPage(pageId);

        return Result.ok();
    }

    @GetMapping("getPage")
    public Result<PageInfoDto> getPage(String pageId) {

        return Result.ok(pageService.getBasePage(pageId));
    }

    @GetMapping("listByParentTree")
    public Result<List<PageNodeDto>> listByParentTree(String parentId, String type) {

        return Result.ok(pageService.listByParentTree(parentId, type));
    }
}
