package com.nway.platform.wform.web.vo;

import lombok.Data;

@Data
public class Result<T> {

    private boolean isSucc;

    private String msg;

    private T data;

    public static <T> Result<T> ok() {
        Result<T> result = new Result<>();
        result.setSucc(true);
        return result;
    }

    public static <T> Result<T> ok(T data) {
        Result<T> result = new Result<>();
        result.setSucc(true);
        result.setData(data);
        return result;
    }

    public static <T> Result<T> error(String msg) {
        Result<T> result = new Result<>();
        result.setSucc(false);
        result.setMsg(msg);
        return result;
    }

}
