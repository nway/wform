package com.nway.platform.wform.web.design;

import com.nway.platform.wform.lib.design.dto.PageNodeDto;
import com.nway.platform.wform.lib.design.entity.Module;
import com.nway.platform.wform.lib.design.service.ModuleService;
import com.nway.platform.wform.web.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("module")
public class ModuleController {

    @Autowired
    private ModuleService moduleService;

    @GetMapping("listAll")
    public Result<List<Module>> listModule() {
        return Result.ok(moduleService.listAll());
    }

    @GetMapping("tree")
    public Result<List<PageNodeDto>> tree() {
        List<Module> modules = moduleService.listAll();
        List<PageNodeDto> nodeList = modules.stream().map(module -> {
            PageNodeDto node = new PageNodeDto();
            node.setId(module.getId());
            node.setTitle(module.getTitle());
            node.setParentId("");
            node.setType("module");
            return node;
        }).collect(Collectors.toList());
        return Result.ok(nodeList);
    }
}
