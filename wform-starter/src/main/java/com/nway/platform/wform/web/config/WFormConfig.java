package com.nway.platform.wform.web.config;

import com.nway.spring.jdbc.SqlExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class WFormConfig {

    @Autowired
    @Qualifier("druidDataSource")
    private DataSource dataSource;

    @Bean("sqlExecutor")
    public SqlExecutor sqlExecutor() {
        return new SqlExecutor(dataSource);
    }

}
