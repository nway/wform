package com.nway.platform.wform.web.config;

import com.alibaba.druid.pool.DruidDataSource;
import io.seata.rm.datasource.DataSourceProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.support.JdbcTransactionManager;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

/**
 * @Author 20030058
 * @Date 2020-05-28
 **/
@Configuration
public class DataSourceConfig {

    private final static Logger logger = LoggerFactory.getLogger(DataSourceConfig.class);

    @Component("druidDataSource")
    @ConfigurationProperties("spring.datasource.druid")
    class DruidDataSourceWrapper extends DruidDataSource {

    }

    /**
     * init datasource proxy
     *
     * @Param: druidDataSource datasource bean instance
     * @Return: DataSourceProxy datasource proxy
     */
    @Bean
    @Primary
    @DependsOn("druidDataSource")
    public DataSourceProxy dataSource(DataSource druidDataSource) {
        logger.info("使用seata代理的数据源");
        return new DataSourceProxy(druidDataSource);
    }

    @Bean
    @DependsOn("druidDataSource")
    public JdbcTransactionManager jdbcTransactionManager(DataSource dataSource) {
        return new JdbcTransactionManager(dataSource);
    }
}
