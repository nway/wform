package com.nway.platform.wform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class WFormApplication {
    public static void main(String[] args) {
        SpringApplication.run(WFormApplication.class, args);
    }
}