特别轻量、灵活易用、高可扩展的后端低代码工具，支持基于seata的全局事务。

没有华丽是文字或漂亮的图片，当前，本项目适合真正寻找简单易用低代码工具的朋友。

#模块说明：
wform-lib 后端类库，提供类库试调用的接口。
wform-web 后端web接口。
wform-starter 可启动的包，应用中不建议独立启动。

PageDataService 内部调用时操作数据的主类。
PageDataHandler 业务扩展时需要实现的接口。
SingleValueComponent 单值组件接口，不需要子表的字段类型
MultiValueComponent  多值组件接口，需要子表操作的字段类型

PageService 表单定义数据的访问

PageDataController web接口
    
FormDesignController 对form定义的web操作接口

src/main/resources/sql/wform-MySQL.sql  数据库初始化脚本


https://docs.oracle.com/zh-cn/learn/understanding-reflection-graalvm-native-image/index.html#conclusions

java -agentlib:native-image-agent=config-output-dir=META-INF/native-image -jar wform-starter-0.0.1-SNAPSHOT.jar

native-image --no-fallback -jar wform-starter-0.0.1-SNAPSHOT.jar

https://docs.spring.io/spring-native/docs/current/reference/htmlsingle/#getting-started-native-build-tools