package com.nway.wform;

import com.nway.platform.wform.lib.components.MultiValueComponent;
import com.nway.platform.wform.lib.components.impl.SimpleMultiValueComponent;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class ClassTest {

    @Test
    public void isAssignableFromTest() {

        SimpleMultiValueComponent component = new SimpleMultiValueComponent();

        log.info("{}", (component instanceof MultiValueComponent));
        log.info("{}", component.getClass().getInterfaces());
    }
}
