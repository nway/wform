package com.nway.platform.wform.lib.design.dao;

import com.nway.platform.wform.lib.design.entity.PageField;
import com.nway.spring.jdbc.SqlExecutor;
import com.nway.spring.jdbc.sql.SQL;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PageFieldDao {

    @Autowired
    private SqlExecutor sqlExecutor;

    public void batchInsert(List<PageField> pageFieldList) {
        sqlExecutor.batchInsert(pageFieldList);
    }

    public void batchUpdate(List<PageField> pageFieldList) {
        sqlExecutor.batchUpdateById(pageFieldList);
    }

    public List<PageField> listByPageId(String pageId) {

        return sqlExecutor.queryList(SQL.query(PageField.class).eq(PageField::getOwnerId, pageId).orderBy(PageField::getFieldOrder));
    }

    public void deleteByFieldId(List<String> fieldIds) {
        if (CollectionUtils.isEmpty(fieldIds)) {
            return;
        }
        sqlExecutor.delete(SQL.delete(PageField.class).in(PageField::getFieldId, fieldIds));
    }
}
