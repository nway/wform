package com.nway.platform.wform.lib.design.db.datatype;

import com.nway.platform.wform.lib.design.db.DbMetaData;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DataTypeFactoryBean implements FactoryBean<DatabaseDialect> {

    @Autowired
    private DbMetaData dbMetaData;
    @Value("${wform.dbDialect:}")
    private String dbDialect;

    @Override
    public DatabaseDialect getObject() throws Exception {
        if (dbDialect.length() == 0) {
            String dataTypeName = DatabaseDialect.class.getName();
            dbDialect = dataTypeName.substring(0, dataTypeName.lastIndexOf('.') + 1) + dbMetaData.getDatabaseProductName() + "Dialect";
        }
        return (DatabaseDialect) getClass().getClassLoader().loadClass(dbDialect).getDeclaredConstructor().newInstance();
    }

    @Override
    public Class<?> getObjectType() {

        return DatabaseDialect.class;
    }

    @Override
    public boolean isSingleton() {

        return true;
    }

}
