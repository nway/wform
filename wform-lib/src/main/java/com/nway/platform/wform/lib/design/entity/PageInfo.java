package com.nway.platform.wform.lib.design.entity;

import com.nway.spring.jdbc.annotation.Column;
import com.nway.spring.jdbc.annotation.Table;
import com.nway.spring.jdbc.annotation.enums.ColumnType;
import com.nway.spring.jdbc.sql.fill.StringIdStrategy;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 不是指具体的页面，
 */
@Data
@Table("t_form_page_info")
public class PageInfo extends BaseEntity {

    @Column(type = ColumnType.ID, fillStrategy = StringIdStrategy.class)
    private String id;

    private String parentId;

    private String moduleId;

    /**
     * 英文名
     */
    private String name;

    /**
     * 中文名
     */
    private String title;

    private String tableName;

    /**
     * form、list
     */
    private String type;

    private int status;

    private Boolean isManual;

    private Boolean gtxCreate;

    private Boolean gtxUpdate;

    private Boolean gtxDelete;

    private Boolean editable;

    private String summary;

}
