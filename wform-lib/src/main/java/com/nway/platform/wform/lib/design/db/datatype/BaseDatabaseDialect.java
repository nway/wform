package com.nway.platform.wform.lib.design.db.datatype;

public abstract class BaseDatabaseDialect implements DatabaseDialect {

    @Override
    public String getInteger(String capacity) {

        return "integer(" + capacity + ")";
    }

    @Override
    public String getString(String capacity) {

        return "varchar(" + capacity + ")";
    }

    @Override
    public String getFloat(String capacity) {

        return "float(" + capacity + ")";
    }

    @Override
    public String getDouble(String capacity) {

        return "double(" + capacity + ")";
    }

    @Override
    public String getDecimal(String capacity) {

        return "decimal(" + capacity + ")";
    }

    @Override
    public String getBoolean() {

        return "bit";
    }

    @Override
    public String getDate() {

        return "timestamp";
    }

    @Override
    public String getTimestamp() {

        return "timestamp";
    }
}
