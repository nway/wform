package com.nway.platform.wform.lib.design.entity;

import com.nway.spring.jdbc.annotation.Column;
import com.nway.spring.jdbc.annotation.Table;
import com.nway.spring.jdbc.annotation.enums.ColumnType;
import com.nway.spring.jdbc.sql.fill.StringIdStrategy;
import lombok.Data;

/**
 * 列表页面的字段定义
 */
@Data
@Table("t_form_page_list")
public class ListField extends BaseEntity {

    @Column(type = ColumnType.ID, fillStrategy = StringIdStrategy.class)
    private String id;

    private String pageId;

    private String fieldId;

    private String searchType;

    private String orderType;

    /**
     * 顺序
     */
    private Integer orderNum;

}
