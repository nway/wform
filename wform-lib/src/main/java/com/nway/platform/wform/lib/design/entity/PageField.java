package com.nway.platform.wform.lib.design.entity;

import com.nway.spring.jdbc.annotation.Column;
import com.nway.spring.jdbc.annotation.Table;
import com.nway.spring.jdbc.annotation.enums.ColumnType;
import com.nway.spring.jdbc.sql.fill.StringIdStrategy;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 真正的表单页面用到的字段
 */
@Data
@Table("t_form_page_field")
public class PageField extends BaseEntity {

    @Column(type = ColumnType.ID, fillStrategy = StringIdStrategy.class)
    private String id;

    /**
     * 所属页面id，是具体的页面，可能是最全信息的主页面，也可能是简化信息的子页面
     *
     * 组字段时此字段为组的id，其他字段为所属page id
     */
    private String ownerId;

    private String fieldId;

    /**
     * 是否必填
     */
    private String validator;

    private Integer fieldOrder;

    /**
     * 是否为权限字段，权限字段会在增删改查所有数据操作时自动补入sql
     */
    private Boolean isAuthorityField;
    /**
     * 表单数据显示时是否隐藏，暂不作为现在列表数据的标识，列表字段手动选择确定，被选字段都应显示
     */
    private Boolean isHidden;

    @Column(type = ColumnType.IGNORE)
    private Boolean isDeleted;

}
