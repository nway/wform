package com.nway.platform.wform.lib.access.validator;

import org.springframework.stereotype.Component;

@Component("noneValidator")
public class NoneValidator implements Validator {

    @Override
    public String validate(String fieldName, Object fieldData) {
        return null;
    }
}
