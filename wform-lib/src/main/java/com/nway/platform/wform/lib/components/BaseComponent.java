package com.nway.platform.wform.lib.components;

public interface BaseComponent {

    Class<?> getJavaType();
}
