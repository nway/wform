package com.nway.platform.wform.lib.design.service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;
import com.nway.platform.wform.lib.access.BeanBuilder;
import com.nway.platform.wform.lib.access.validator.ValidatorRegister;
import com.nway.platform.wform.lib.commons.utils.BeanUtils;
import com.nway.platform.wform.lib.components.ComponentRegister;
import com.nway.platform.wform.lib.components.FieldGroupComponent;
import com.nway.platform.wform.lib.components.MultiValueComponent;
import com.nway.platform.wform.lib.components.impl.KeyComponent;
import com.nway.platform.wform.lib.design.constants.PageTypeConstants;
import com.nway.platform.wform.lib.design.dao.*;
import com.nway.platform.wform.lib.design.db.TableGenerator;
import com.nway.platform.wform.lib.design.db.TableService;
import com.nway.platform.wform.lib.design.dto.*;
import com.nway.platform.wform.lib.design.dto.save.PageFieldSaveDto;
import com.nway.platform.wform.lib.design.entity.*;
import com.nway.platform.wform.lib.design.query.PageListQuery;
import com.nway.platform.wform.lib.design.utils.FormFieldHelper;
import com.nway.platform.wform.lib.design.utils.StringUtils;
import com.nway.platform.wform.lib.exception.WFormException;
import com.nway.spring.jdbc.pagination.Page;
import com.nway.spring.jdbc.sql.fill.incrementer.IdWorker;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 页面定义服务
 */
@Slf4j
@Service
public class PageService {

    private final static Cache<String, BasePage> PAGE_DTO_CACHE = CacheBuilder.newBuilder()
            .maximumSize(500)
            .expireAfterAccess(10, TimeUnit.DAYS)
            .build();

    private final static Cache<String, String> PAGE_ID_NAME_MAP_CACHE = CacheBuilder.newBuilder()
            .maximumSize(500)
            .expireAfterAccess(10, TimeUnit.DAYS)
            .build();

    private final static Cache<String, Map<String, Map<String, String>>> FIELD_META_CACHE = CacheBuilder.newBuilder()
            .maximumSize(1000)
            .expireAfterAccess(10, TimeUnit.DAYS)
            .build();

    private final static Cache<String, Class<?>> PAGE_BEAN_CACHE = CacheBuilder.newBuilder()
            .maximumSize(1000)
            .expireAfterAccess(10, TimeUnit.DAYS)
            .build();

    @Autowired
    private PageDao pageDao;
    @Autowired
    private FormFieldDao formFieldDao;
    @Autowired
    private FormFieldAttrDao fieldAttrDao;
    @Autowired
    private PageFieldDao pageFieldDao;
    @Autowired
    private ListPageDao listPageDao;
    @Autowired
    private ModuleDao moduleDao;
    @Autowired
    private TableService tableService;
    @Autowired
    private ComponentRegister componentRegister;
    @Autowired
    private ValidatorRegister validatorRegister;
    @Autowired
    private TableGenerator tableGenerator;

    public BasePage getById(String id) {
        try {
            return PAGE_DTO_CACHE.get(id, () -> doGetFormPage(id));
        } catch (ExecutionException e) {
            log.error("获取页面定义失败", e);
        }
        return null;
    }

    public BasePage getByName(String name) {
        try {
            final String pageId = getPageId(name);
            return PAGE_DTO_CACHE.get(pageId, () -> doGetFormPage(pageId));
        } catch (ExecutionException e) {
            log.error("获取页面定义失败", e);
        }
        return null;
    }

    public List<PageFieldDto> getAuthorityField(String name) {
        BasePage page = getByName(name);
        return page.getFields().stream().filter(PageFieldDto::getIsAuthorityField).collect(Collectors.toList());
    }

    public List<PageNodeDto> listByParentTree(String parentId, String type) {
        List<PageInfo> parentList = Collections.emptyList();
        if ("module".equals(type)) {
            parentList = pageDao.listByModuleId(parentId);
        } else if ("page".equals(type)) {
            parentList = pageDao.listByParent(parentId);
        }

        return parentList.stream().map(page -> {
            PageNodeDto node = new PageNodeDto();
            node.setId(page.getId());
            node.setTitle(page.getTitle());
            node.setParentId(page.getParentId());
            node.setType("page");
            return node;
        }).collect(Collectors.toList());
    }

    public Page<ListPageDto> pageList(PageListQuery query) {

        Page<PageInfo> page = pageDao.pageList(query);

        return BeanUtils.copyToPage(page, ListPageDto.class);
    }

    public Class<?> getBeanClass(BasePage page) {
        try {
            return PAGE_BEAN_CACHE.get(page.getName(), () -> BeanBuilder.createPageClass(page));
        } catch (ExecutionException e) {
            log.error("创建页面bean类型异常", e);
        }
        return null;
    }

    private BasePage doGetFormPage(String id) {

        PageInfo page = pageDao.getById(id);

        BasePage pageDto;
        if (PageTypeConstants.PAGE_TYPE_LIST.equals(page.getType())) {
            pageDto = BeanUtils.copy(page, ListPageDto.class);
        } else if (PageTypeConstants.PAGE_TYPE_FORM.equals(page.getType())) {
            pageDto = BeanUtils.copy(page, FormPageDto.class);
        } else if (PageTypeConstants.PAGE_TYPE_NESTED.equals(page.getType())) {
            pageDto = BeanUtils.copy(page, NestedPageDto.class);
        } else {
            throw new WFormException("无效的页面类型,type = " + page.getType());
        }

        String moduleName = moduleDao.getNameById(pageDto.getModuleId());
        pageDto.setModuleName(moduleName);

        String pageId = page.getId();
        // 只要list页面的字段可以继承自所述表单页面
        if (PageTypeConstants.PAGE_TYPE_LIST.equals(pageDto.getType())) {
            pageId = StringUtils.blankToDefault(page.getParentId(), page.getId());
        }

        List<PageFieldDto> fieldDtoList = listPageField(pageId);
        for (PageFieldDto field : fieldDtoList) {
            if (field.getComponentObj() instanceof KeyComponent) {
                pageDto.setKeyField(field);
                break;
            }
        }
        pageDto.setFields(fieldDtoList);

        if (PageTypeConstants.PAGE_TYPE_LIST.equals(page.getType())) {
            doGetPageList((ListPageDto) pageDto);
        }

        return pageDto;
    }

    private void doGetPageList(ListPageDto listPageDto) {

        Map<String, PageFieldDto> pageFieldDtoMap = listPageDto.getFields().stream().collect(Collectors.toMap(PageFieldDto::getId, Function.identity()));

        List<ListField> listPages = listPageDao.listByPageId(listPageDto.getId());
        List<PageFieldDto> listPageFields = listPages.stream().map(e -> pageFieldDtoMap.get(e.getFieldId())).collect(Collectors.toList());
        listPageDto.setFields(listPageFields);

        List<ListPageFieldDto> listPageConditionFields = listPages.stream().filter(field -> StringUtils.isNotBlank(field.getSearchType()))
                .map(field -> {
                    PageFieldDto formFieldDto = pageFieldDtoMap.get(field.getFieldId());
                    ListPageFieldDto listPageFieldDto = BeanUtils.copy(formFieldDto, ListPageFieldDto.class);
                    listPageFieldDto.setSearchType(field.getSearchType());
                    listPageFieldDto.setComponentObj(formFieldDto.getComponentObj());
                    listPageFieldDto.setFieldAttr(formFieldDto.getFieldAttr());
                    return listPageFieldDto;
                }).collect(Collectors.toList());
        listPageDto.setConditionField(listPageConditionFields);

        List<ListPageFieldDto> listPageOrderFields = listPages.stream().filter(field -> StringUtils.isNotBlank(field.getOrderType()))
                .map(field -> {
                    PageFieldDto formFieldDto = pageFieldDtoMap.get(field.getFieldId());
                    ListPageFieldDto listPageFieldDto = BeanUtils.copy(formFieldDto, ListPageFieldDto.class);
                    listPageFieldDto.setOrderType(field.getOrderType());
                    listPageFieldDto.setComponentObj(formFieldDto.getComponentObj());
                    listPageFieldDto.setFieldAttr(formFieldDto.getFieldAttr());
                    return listPageFieldDto;
                }).filter(field -> !Optional.ofNullable(field.getIsMultiValue()).orElse(Boolean.FALSE)).collect(Collectors.toList());
        listPageDto.setOrderField(listPageOrderFields);
    }

    /**
     * 子页面的字段继承自父级页面，只支持二级子页面
     *
     * @param pageId
     * @return
     */
    public List<PageFieldDto> listPageField(String pageId) {

        // 获取字段定义 key：fieldId
        Map<String, FormField> fieldMap = formFieldDao.mapByPageId(pageId);
        // 获取页面字段列表
        List<PageField> pageFieldList = pageFieldDao.listByPageId(pageId);

        Map<String, Map<String, String>> fieldMeta = doListFieldMeta(pageId);

        List<PageFieldDto> fieldDtoList = BeanUtils.copyToList(pageFieldList, PageFieldDto.class);
        // 合并字段定义和页面字段定义
        for (PageFieldDto pageFieldDto : fieldDtoList) {
            FormField formField = fieldMap.get(pageFieldDto.getFieldId());
            pageFieldDto.setDisplay(formField.getDisplay());
            pageFieldDto.setSize(formField.getSize());
            pageFieldDto.setValidatorObj(validatorRegister.getValidator(pageFieldDto.getValidator()));
            pageFieldDto.setComponent(formField.getComponent());
            pageFieldDto.setComponentObj(componentRegister.getComponent(formField.getComponent()));
            pageFieldDto.setName(formField.getName());
            pageFieldDto.setNameCamel(StringUtils.convertToLowCamel(formField.getName()));
            pageFieldDto.setNameUnder(StringUtils.convertToLowUnder(formField.getName()));
            pageFieldDto.setIsMultiValue(Boolean.FALSE);
            // 属性绑定在formField上
            pageFieldDto.setFieldAttr(fieldMeta.get(pageFieldDto.getName()));
            if (pageFieldDto.getComponentObj() instanceof MultiValueComponent) {
                pageFieldDto.setIsMultiValue(true);
            }
        }

        // 获取组类型字段
        List<PageFieldDto> groupFields = fieldDtoList.stream().filter(fieldDto -> fieldDto.getComponentObj() instanceof FieldGroupComponent).collect(Collectors.toList());

        // PageFieldDto ==> PageFieldGroupDto
        for (PageFieldDto fieldDto : groupFields) {
            PageFieldGroupDto groupDto = new PageFieldGroupDto();
            BeanUtils.copy(fieldDto, groupDto);
            String tableName = getFieldMeta(fieldDto.getOwnerId(), fieldDto.getName(), "tableName");
            groupDto.setTableName(tableName);
            fieldDtoList.remove(fieldDto);
            fieldDtoList.add(groupDto);
        }

        // 属性可以绑在页面字段上
        List<PageFieldGroupDto> groupFields2 = fieldDtoList.stream()
                .filter(fieldDto -> fieldDto.getComponentObj() instanceof FieldGroupComponent)
                .map(fieldDto -> (PageFieldGroupDto) fieldDto)
                .collect(Collectors.toList());

        // 并发递归页面内所有组字段
        groupFields2.parallelStream().forEach(groupField -> {
            List<PageFieldDto> fields = listPageField(groupField.getId());
            groupField.setFields(fields);
        });

        return fieldDtoList;
    }

    public PageInfoDto getBasePage(String pageId) {
        return BeanUtils.copy(pageDao.getById(pageId), PageInfoDto.class);
    }

    public String getPageId(String pageName) {
        try {
            return PAGE_ID_NAME_MAP_CACHE.get(pageName, () -> pageDao.getIdByName(pageName));
        } catch (ExecutionException e) {
            log.error("根据name获取id失败", e);
        }
        return null;
    }

    public Map<String, Map<String, String>> listFieldMeta(String pageId) {
        try {
            return FIELD_META_CACHE.get(pageId, () -> doListFieldMeta(pageId));
        } catch (ExecutionException e) {
            log.error("获取字段配置数据失败", e);
        }
        return null;
    }

    public Map<String, String> getFieldMeta(String pageId, String fieldName) {
        try {
            return FIELD_META_CACHE.get(pageId, () -> doListFieldMeta(pageId)).get(fieldName);
        } catch (ExecutionException e) {
            log.error("获取字段配置数据失败", e);
        }
        return null;
    }

    public String getFieldMeta(String pageId, String fieldName, String attrName) {
        try {
            return FIELD_META_CACHE.get(pageId, () -> doListFieldMeta(pageId)).get(fieldName).get(attrName);
        } catch (ExecutionException e) {
            log.error("获取字段配置数据失败", e);
        }
        return null;
    }

    /**
     * Map<fieldName, Map<attrName, attrValue>>
     *
     * @param pageId
     * @return
     */
    private Map<String, Map<String, String>> doListFieldMeta(String pageId) {

        List<FormField> formFields = formFieldDao.listByPageId(pageId);

        Map<String, String> fieldIdNameMap = formFields.stream().collect(Collectors.toMap(FormField::getId, FormField::getName));
        List<FormFieldAttr> attrList = fieldAttrDao.listByFieldId(formFields.stream().map(FormField::getId).collect(Collectors.toList()));

        Map<String, Map<String, String>> retVal = new HashMap<>();
        for (FormFieldAttr row : attrList) {
            Map<String, String> groupRow = retVal.get(fieldIdNameMap.get(row.getFieldId()));
            if (groupRow != null) {
                groupRow.put(row.getAttrName(), row.getAttrValue());
            } else {
                groupRow = new HashMap<>();
                groupRow.put(row.getAttrName(), row.getAttrValue());
                retVal.put(fieldIdNameMap.get(row.getFieldId()), groupRow);
            }
        }

        return retVal;
    }

    public void savePage(PageInfoDto pageInfo) {

        PageInfo page = BeanUtils.copy(pageInfo, PageInfo.class);

        if (StringUtils.isBlank(page.getId())) {
            pageDao.insert(page);
        } else {
            pageDao.update(page);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveFormFields(String pageId, List<PageFieldSaveDto> fields) {

        List<FormField> formFieldInsertList = new ArrayList<>();
        List<FormField> formFieldUpdateList = new ArrayList<>();

        List<PageField> pageFieldInsertList = new ArrayList<>();
        List<PageField> pageFieldUpdateList = new ArrayList<>();

        List<FormField> formFieldDeleteList = new ArrayList<>();

        List<FormFieldAttr> formFieldAttrList = new ArrayList<>();

        PageInfo pageInfo = Optional.ofNullable(pageDao.getById(pageId)).orElse(new PageInfo());
        String moduleId = pageInfo.getModuleId();

        List<String> fieldIds = fields.stream().map(PageFieldSaveDto::getFieldId).collect(Collectors.toList());
        List<String> groupFieldIds = fields.stream().filter(field -> field.getGroupFields() != null).flatMap(field -> field.getGroupFields().stream()).map(PageFieldSaveDto::getFieldId).collect(Collectors.toList());
        fieldIds.addAll(groupFieldIds);

        // 当前数据库里的表单字段
        List<FormField> formFields = formFieldDao.listByPageId(pageId);
        Map<String, String> fieldNameMap = formFields.stream().collect(Collectors.toMap(FormField::getId, FormField::getName));

        outer:
        for (FormField formField : formFields) {
            for (PageFieldSaveDto field : fields) {
                // 能在现有表找到字段对应的id  说明不需要删除
                if (formField.getId().equals(field.getFieldId())) {
                    continue outer;
                }
            }
            formFieldDeleteList.add(formField);
        }

        for (int i = 0; i < fields.size(); i++) {

            PageFieldSaveDto pageFieldSaveDto = fields.get(i);

            FormField formField = new FormField();
            BeanUtils.copy(pageFieldSaveDto, formField);
            formField.setId(pageFieldSaveDto.getFieldId());
            formField.setOwnerId(pageId);

            if (!fieldNameMap.getOrDefault(formField.getId(), formField.getName()).equals(formField.getName())) {
                formField.setOldName(fieldNameMap.get(formField.getId()));
            }

            // 基础的字段定义不存在  则页面中字段也应不存在
            if (StringUtils.isBlank(formField.getId()) || !fieldNameMap.containsKey(formField.getId())) {
                formField.setId(IdWorker.getIdStr());
                formFieldInsertList.add(formField);
            } else {
                formFieldUpdateList.add(formField);
            }

            PageField pageField = new PageField();
            pageField.setId(pageFieldSaveDto.getId());
            pageField.setFieldId(formField.getId());
            pageField.setOwnerId(pageId);
            pageField.setValidator(pageFieldSaveDto.getValidator());
            pageField.setFieldOrder(i);

            if (StringUtils.isBlank(pageField.getId())) {
                pageField.setId(formField.getId());
                pageFieldInsertList.add(pageField);
            } else {
                pageFieldUpdateList.add(pageField);
            }

            Map<String, String> options = pageFieldSaveDto.getOptions();
            if (MapUtils.isNotEmpty(options)) {
                fieldAttrDao.delete(Lists.newArrayList(pageFieldSaveDto.getFieldId()));
                for (Entry<String, String> entry : options.entrySet()) {
                    FormFieldAttr fieldAttr = new FormFieldAttr();
                    fieldAttr.setFieldId(formField.getId());
                    fieldAttr.setAttrName(entry.getKey());
                    fieldAttr.setAttrValue(entry.getValue());
                    formFieldAttrList.add(fieldAttr);
                }
            }

            // 字段组对应t_form_page_info nested类型数据
            if (CollectionUtils.isNotEmpty(pageFieldSaveDto.getGroupFields())) {
                PageInfo pageInfoInner = new PageInfo();
                pageInfoInner.setId(pageField.getId());
                pageInfoInner.setParentId(pageId);
                pageInfoInner.setModuleId(moduleId);
                pageInfoInner.setType(PageTypeConstants.PAGE_TYPE_NESTED);
                pageInfoInner.setTableName(options.get("tableName"));
                pageInfoInner.setName(pageFieldSaveDto.getName());
                pageInfoInner.setTitle(pageFieldSaveDto.getDisplay());
                pageDao.insert(pageInfoInner);

                saveFormFields(pageInfoInner.getId(), pageFieldSaveDto.getGroupFields());
            }
        }

        List<String> deletedFormIds = formFieldDeleteList.stream().map(FormField::getId).collect(Collectors.toList());

        formFieldDao.deleteById(deletedFormIds);
        pageFieldDao.deleteByFieldId(deletedFormIds);

        formFieldDao.insert(formFieldInsertList);
        fieldAttrDao.insert(formFieldAttrList);
        pageFieldDao.batchInsert(pageFieldInsertList);

        formFieldDao.update(formFieldUpdateList);
        pageFieldDao.batchUpdate(pageFieldUpdateList);

        tableService.alterColumn(pageInfo.getTableName(), formFieldInsertList, formFieldUpdateList);

        PAGE_DTO_CACHE.invalidate(pageId);
        FIELD_META_CACHE.invalidate(pageId);
    }

    /**
     * 列表页面需要有对应的page_field、form_field、page_info定义
     */
    public void savePageList() {

    }

    public void publishPage(String pageId) {

        BasePage page = getById(pageId);

        if (!page.getClass().isAssignableFrom(FormPageDto.class)) {
            log.warn("不是表单页面，不需要发布");
            return;
        }

        if (!Optional.ofNullable(((FormPageDto) page).getIsManual()).orElse(Boolean.TRUE)) {
            tableGenerator.createTable(page);
        }

        for (PageFieldDto field : FormFieldHelper.getMultiValueFormFields(page.getFields())) {
            MultiValueComponent component = (MultiValueComponent) field.getComponentObj();
            component.onPublish(field);
        }
    }

    public List<PageFieldDto> listFields(String pageId) {

        return BeanUtils.copyToList(formFieldDao.listByPageId(pageId), PageFieldDto.class);
    }

}
