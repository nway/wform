package com.nway.platform.wform.lib.access.validator;

public interface Validator {

    String validate(String fieldName, Object fieldData);
}
