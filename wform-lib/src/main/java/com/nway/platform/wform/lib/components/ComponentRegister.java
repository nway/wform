package com.nway.platform.wform.lib.components;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class ComponentRegister implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    private final Map<String, BaseComponent> components = new LinkedHashMap<>();

    public BaseComponent getComponent(String name) {
        String beanName = name + "Component";
        return Optional.ofNullable(components.get(beanName + "Ext"))
                .orElseGet(() -> Optional.ofNullable(components.get(beanName))
                        .orElseGet(() -> {
                            BaseComponent bean = applicationContext.getBean(beanName, BaseComponent.class);
                            this.components.put(beanName, bean);
                            return bean;
                        }));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

        this.applicationContext = applicationContext;
    }

}
