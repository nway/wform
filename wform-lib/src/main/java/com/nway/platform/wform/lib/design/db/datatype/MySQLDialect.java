package com.nway.platform.wform.lib.design.db.datatype;

public class MySQLDialect extends BaseDatabaseDialect {

    @Override
    public String getDate() {
        return "datetime";
    }

    @Override
    public String getTimestamp() {
        return "datetime";
    }
}