package com.nway.platform.wform.lib.access;

import com.nway.platform.wform.lib.design.dto.BasePage;
import com.nway.platform.wform.lib.design.dto.PageFieldDto;
import com.nway.spring.jdbc.bean.processor.asm.DynamicBeanClassLoader;
import org.objectweb.asm.*;
import org.springframework.util.ClassUtils;

import java.util.List;

public abstract class BeanBuilder {

    private static final String BEAN_PACKAGE = "com/nway/platform/wform/lib/access/bean";

    public static Class<?> createPageClass(BasePage page) {

        ClassWriter classWriter = new ClassWriter(0);
        FieldVisitor fieldVisitor;
        MethodVisitor methodVisitor;
        AnnotationVisitor annotationVisitor0;
        String className = BEAN_PACKAGE + "/" + upperFirst(page.getName());

        classWriter.visit(Opcodes.V1_8, Opcodes.ACC_PUBLIC | Opcodes.ACC_SUPER, className, null, "java/lang/Object", null);

        classWriter.visitSource(upperFirst(page.getName()) + ".java", null);

        {
            annotationVisitor0 = classWriter.visitAnnotation("Lcom/nway/spring/jdbc/annotation/Table;", true);
            annotationVisitor0.visit("value", page.getTableName());
            annotationVisitor0.visitEnd();
        }

        List<PageFieldDto> formFields = page.getFields();
        // 类属性
        for (PageFieldDto formField : formFields) {
            {
                fieldVisitor = classWriter.visitField(Opcodes.ACC_PRIVATE, formField.getNameCamel(), getDescriptor(formField.getComponentObj().getJavaType()), null, null);

                annotationVisitor0 = fieldVisitor.visitAnnotation("Lcom/nway/spring/jdbc/annotation/Column;", true);
                annotationVisitor0.visit("name", formField.getNameUnder());
                if (page.getKeyField() == formField) {
                    annotationVisitor0.visitEnum("type", "Lcom/nway/spring/jdbc/annotation/enums/ColumnType;", "ID");
                } else if (formField.getIsMultiValue()) {
                    annotationVisitor0.visitEnum("type", "Lcom/nway/spring/jdbc/annotation/enums/ColumnType;", "IGNORE");
                }
                annotationVisitor0.visitEnd();
                fieldVisitor.visitEnd();
            }
        }
        {
            methodVisitor = classWriter.visitMethod(Opcodes.ACC_PUBLIC, "<init>", "()V", null, null);
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(13, label0);
            methodVisitor.visitVarInsn(Opcodes.ALOAD, 0);
            methodVisitor.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
            methodVisitor.visitInsn(Opcodes.RETURN);
            Label label1 = new Label();
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLocalVariable("this", "L" + className + ";", null, label0, label1, 0);
            methodVisitor.visitMaxs(1, 1);
            methodVisitor.visitEnd();
        }

        int lineNumber = formFields.size() * 2;

        for (PageFieldDto formField : formFields) {

            String beanClass = getDescriptor(formField.getComponentObj().getJavaType());

            // get
            methodVisitor = classWriter.visitMethod(Opcodes.ACC_PUBLIC, getter(formField), "()" + beanClass, null, null);
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(lineNumber++, label0);
            methodVisitor.visitVarInsn(Opcodes.ALOAD, 0);
            methodVisitor.visitFieldInsn(Opcodes.GETFIELD, className, formField.getNameCamel(), beanClass);
            methodVisitor.visitInsn(Opcodes.ARETURN);
            Label label1 = new Label();
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLocalVariable("this", "L" + className + ";", null, label0, label1, 0);
            methodVisitor.visitMaxs(1, 1);
            methodVisitor.visitEnd();

            // set
            methodVisitor = classWriter.visitMethod(Opcodes.ACC_PUBLIC, setter(formField), "(" + beanClass + ")V", null, null);
            methodVisitor.visitCode();
            Label label2 = new Label();
            methodVisitor.visitLabel(label2);
            methodVisitor.visitLineNumber(lineNumber = lineNumber + 4, label2);
            methodVisitor.visitVarInsn(Opcodes.ALOAD, 0);
            methodVisitor.visitVarInsn(Opcodes.ALOAD, 1);
            methodVisitor.visitFieldInsn(Opcodes.PUTFIELD, className, formField.getNameCamel(), beanClass);
            Label label3 = new Label();
            methodVisitor.visitLabel(label3);
            methodVisitor.visitLineNumber(lineNumber++, label3);
            methodVisitor.visitInsn(Opcodes.RETURN);
            Label label4 = new Label();
            methodVisitor.visitLabel(label4);
            methodVisitor.visitLocalVariable("this", "L" + className + ";", null, label2, label4, 0);
            methodVisitor.visitLocalVariable(formField.getNameCamel(), beanClass, null, label2, label4, 1);
            methodVisitor.visitMaxs(2, 2);
            methodVisitor.visitEnd();
        }
        classWriter.visitEnd();

        try {
            DynamicBeanClassLoader beanClassLoader = new DynamicBeanClassLoader(ClassUtils.getDefaultClassLoader());
            return beanClassLoader.defineClass(className.replace('/', '.'), classWriter.toByteArray());
        } catch (Exception e) {
            throw new RuntimeException("使用ASM创建 [ " + className + " ] 失败", e);
        }
    }

    /**
     * 实体类set方法的参数类型
     *
     * @param clazz
     * @return
     */
    private static String getDescriptor(Class<?> clazz) {
        if (Integer.TYPE.equals(clazz)) {
            return "I";
        } else if (Long.TYPE.equals(clazz)) {
            return "J";
        } else if (Float.TYPE.equals(clazz)) {
            return "F";
        } else if (Double.TYPE.equals(clazz)) {
            return "D";
        } else if (Boolean.TYPE.equals(clazz)) {
            return "Z";
        } else if (Byte.TYPE.equals(clazz)) {
            return "B";
        } else if (Short.TYPE.equals(clazz)) {
            return "S";
        } else if (byte[].class.equals(clazz)) {
            return "[B";
        }
        return "L" + clazz.getName().replace('.', '/') + ";";
    }

    private static String setter(PageFieldDto field) {
        String fieldName = field.getNameCamel();
        if (field.getComponentObj().getJavaType() == Boolean.TYPE && fieldName.startsWith("is")) {
            return "set" + fieldName.substring(2);
        }
        return "set" + upperFirst(fieldName);
    }

    private static String getter(PageFieldDto field) {
        String fieldName = field.getNameCamel();
        if (field.getComponentObj().getJavaType() == Boolean.TYPE && fieldName.startsWith("is")) {
            return fieldName;
        }
        return "get" + upperFirst(fieldName);
    }

    private static String upperFirst(String name) {
        char[] chars = name.toCharArray();
        return Character.toUpperCase(chars[0]) + new String(chars, 1, chars.length - 1);
    }

}
