package com.nway.platform.wform.lib.design.query;

import lombok.Data;

@Data
public class PageListQuery extends PageParam {

    private String title;

    private String keyId;

    private String type;

}
