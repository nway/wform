package com.nway.platform.wform.lib.components;

public interface SingleValueComponent extends BaseComponent {

    /**
     * 获取此组件预期的数据库字段类型，双精度模式使用英文逗号分隔
     *
     * @param capacity 数据长度
     * @return
     */
    String getSqlType(String capacity);

    /**
     * 根据组件所需的实际类型，转换页面参数值
     * <p>
     * 比如对于日期组件，前端传值2021-09-10 09:51:00，则转为java Date、LocalDateTime等java日期类型对象
     * </p>
     *
     * @param value 页面参数
     * @return 组件支持的java类型
     */
    Object getValue(Object value);

}
