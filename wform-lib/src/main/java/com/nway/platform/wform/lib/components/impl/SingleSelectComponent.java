package com.nway.platform.wform.lib.components.impl;

import com.nway.platform.wform.lib.components.Initializable;
import com.nway.platform.wform.lib.components.SingleValueComponent;
import com.nway.platform.wform.lib.design.db.datatype.DatabaseDialect;
import com.nway.platform.wform.lib.design.dto.PageFieldDto;
import com.nway.platform.wform.lib.design.service.PageService;
import com.nway.spring.jdbc.SqlExecutor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component("singleSelectComponent")
public class SingleSelectComponent implements SingleValueComponent, Initializable {

    @Autowired
    private DatabaseDialect databaseDialect;
    @Autowired
    private PageService formPageAccess;
    @Autowired
    private SqlExecutor sqlExecutor;

    @Override
    public Class<?> getJavaType() {
        return Map.class;
    }

    @Override
    public Object getValue(Object value) {
        return value;
    }

    @Override
    public Object init(PageFieldDto field) {

        Map<String, Object> map = new HashMap<>();

        Map<String, Map<String, String>> fieldAttrsMap = formPageAccess.listFieldMeta(field.getOwnerId());

        if (fieldAttrsMap != null) {

            Map<String, String> attrMap = fieldAttrsMap.get(field.getName());

            String sql = attrMap.get("sql");
            if (StringUtils.isNotBlank(sql)) {
                map = reform(sqlExecutor.getJdbcTemplate().queryForList(sql));
            }
        }
        return map;
    }


    private Map<String, Object> reform(List<Map<String, Object>> origin) {
        Map<String, Object> kvMap = new HashMap<>();
        for (Map<String, Object> per : origin) {
            kvMap.put(per.get("key").toString(), per.get("value"));
        }
        return kvMap;
    }

    @Override
    public String getSqlType(String capacity) {
        return databaseDialect.getString(capacity);
    }

}
