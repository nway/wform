package com.nway.platform.wform.lib.components.impl;

import com.nway.platform.wform.lib.components.SingleValueComponent;
import com.nway.platform.wform.lib.design.db.datatype.DatabaseDialect;
import com.nway.platform.wform.lib.exception.WFormException;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Date;

@Component("dateComponent")
public class DateComponent implements SingleValueComponent {

    @Autowired
    private DatabaseDialect databaseDialect;

    @Override
    public String getSqlType(String capacity) {
        return databaseDialect.getTimestamp();
    }

    @Override
    public Object getValue(Object value) {
        if (value == null || value.toString().length() == 0) {
            return value;
        }
        try {
            return DateUtils.parseDate((String) value, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd");
        } catch (ParseException e) {
            throw new WFormException(e);
        }
    }

    @Override
    public Class<?> getJavaType() {
        return Date.class;
    }
}
