package com.nway.platform.wform.lib.commons.constant;

public class SearchTypeConstant {
    public static final String like = "like";
    public static final String eq = "eq";
    public static final String in = "in";
    public static final String rightLike = "right-like";
}
