package com.nway.platform.wform.lib.design.dao;

import com.nway.platform.wform.lib.design.entity.PageInfo;
import com.nway.platform.wform.lib.design.query.PageListQuery;
import com.nway.spring.jdbc.SqlExecutor;
import com.nway.spring.jdbc.pagination.Page;
import com.nway.spring.jdbc.sql.SQL;
import com.nway.spring.jdbc.sql.builder.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PageDao {

    @Autowired
    private SqlExecutor sqlExecutor;

    public PageInfo getById(String id) {
        return sqlExecutor.queryById(id, PageInfo.class);
    }

    public void insert(PageInfo pageInfo) {
        sqlExecutor.insert(pageInfo);
    }

    public void update(PageInfo pageInfo) {
        sqlExecutor.updateById(pageInfo);
    }

    public String getIdByName(String pageName) {
        return sqlExecutor.<PageInfo>queryFirst(SQL.query(PageInfo.class).withColumn(PageInfo::getId).eq(PageInfo::getName, pageName)).getId();
    }

    public List<PageInfo> listByParent(String parentId) {
        QueryBuilder queryBuilder = SQL.query(PageInfo.class)
                .eq(PageInfo::getParentId, parentId)
                .orderByDesc(PageInfo::getUpdateTime);
        return sqlExecutor.queryList(queryBuilder);
    }

    public List<PageInfo> listByModuleId(String moduleId) {
        QueryBuilder queryBuilder = SQL.query(PageInfo.class)
                .eq(PageInfo::getModuleId, moduleId)
                .orderByDesc(PageInfo::getUpdateTime);
        return sqlExecutor.queryList(queryBuilder);
    }

    public Page<PageInfo> pageList(PageListQuery query) {
        QueryBuilder queryBuilder = SQL.query(PageInfo.class).ignoreInvalidDeep(true);

        if ("module".equals(query.getType())) {
            queryBuilder.eq(PageInfo::getModuleId, query.getKeyId());
        } else if ("page".equals(query.getType())) {
            queryBuilder.eq(PageInfo::getParentId, query.getKeyId());
        }

        queryBuilder.like(PageInfo::getTitle, query.getTitle());
        return sqlExecutor.queryPage(queryBuilder, query.getPageNum(), query.getPageSize());
    }
}
