package com.nway.platform.wform.lib.design.entity;

import com.nway.spring.jdbc.annotation.Column;
import com.nway.spring.jdbc.annotation.Table;
import com.nway.spring.jdbc.annotation.enums.ColumnType;
import com.nway.spring.jdbc.sql.fill.StringIdStrategy;
import lombok.Data;

/**
 * 页面字段的定义，与页面无关的基础信息
 */
@Data
@Table("t_form_field")
public class FormField extends BaseEntity {
    @Column(type = ColumnType.ID, fillStrategy = StringIdStrategy.class)
    private String id;
    private String ownerId;
    private String name;
    @Column(type = ColumnType.IGNORE)
    private String oldName;
    private String display;
    private String size;
    private Integer status;
    private String component;
    @Column(type = ColumnType.IGNORE)
    private Boolean isDeleted;
}
