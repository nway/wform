package com.nway.platform.wform.lib.components.impl.dao;

import com.nway.spring.jdbc.SqlExecutor;
import com.nway.spring.jdbc.sql.fill.incrementer.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class CheckboxDao {

    @Autowired
    private SqlExecutor sqlExecutor;

    public List<Object> listBizId(String tableName, List<Object> value) {
        String sql = "select biz_id from " + tableName + " where value in (" + IntStream.range(0, value.size()).mapToObj((o) -> "?").collect(Collectors.joining(",")) + ")";
        return sqlExecutor.getJdbcTemplate().queryForList(sql, Object.class, value.toArray());
    }

    public List<Map<String, Object>> getValues(String tableName, List<String> bizIdList) {
        String sql = " select biz_id, value from " + tableName + " where biz_id in (" + IntStream.range(0, bizIdList.size()).mapToObj((o) -> "?").collect(Collectors.joining(",")) + ")";
        return sqlExecutor.getJdbcTemplate().queryForList(sql, bizIdList.toArray());
    }

    public void save(String tableName, String bizId, Object value) {
        if (!(value instanceof List)) {
            throw new IllegalArgumentException("需要List格式数据");
        }
        String sql = " insert into " + tableName + " (ID,BIZ_ID, VALUE) values (?,?,?)";
        sqlExecutor.getJdbcTemplate().batchUpdate(sql, ((List<Object>) value).stream().map(data -> {
            Object[] param = new Object[3];
            param[0] = IdWorker.get32UUID();
            param[1] = bizId;
            param[2] = data;
            return param;
        }).collect(Collectors.toList()));
    }

    public void clear(String tableName, List<String> bizId) {
        String sql = "delete from " + tableName + " where biz_id in (" + IntStream.range(0, bizId.size()).mapToObj((o) -> "?").collect(Collectors.joining(",")) + ")";
        sqlExecutor.getJdbcTemplate().update(sql, bizId.toArray());
    }
}
