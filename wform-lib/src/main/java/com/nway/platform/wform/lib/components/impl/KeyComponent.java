package com.nway.platform.wform.lib.components.impl;

import com.nway.platform.wform.lib.components.SingleValueComponent;
import com.nway.platform.wform.lib.design.db.datatype.DatabaseDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

/**
 * 主键组件，默认是UUID类型，如果要实现其他类型，需要继承此类。
 */
@Component("keyComponent")
public class KeyComponent implements SingleValueComponent {
	
	@Autowired
	private DatabaseDialect databaseDialect;

	@Override
	public Object getValue(Object value) {
		
		return value == null || value.toString().length() == 0 ? UUID.randomUUID().toString().replace("-","") : value;
	}

	@Override
	public String getSqlType(String capacity) {

		return databaseDialect.getString(Optional.ofNullable(capacity).orElse("36")) + " primary key";
	}

	@Override
	public Class<?> getJavaType() {
		return String.class;
	}
}
