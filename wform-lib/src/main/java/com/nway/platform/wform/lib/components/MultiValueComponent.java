package com.nway.platform.wform.lib.components;

import com.nway.platform.wform.lib.design.dto.PageFieldDto;

import java.util.List;
import java.util.Map;

/**
 * 此组件为复合数据类型提供支持，适用于使用子表解决问题的场景
 *
 * @author zdtjss@163.com
 */
public interface MultiValueComponent extends BaseComponent {

    /**
     * 获取父级组件业务数据主键，父级组件是指当前组件所属的组件
     *
     * @param field 关联字段
     * @param val   页面传值
     * @return
     */
    Object getParentId(PageFieldDto field, Object val);

    /**
     * 将页面传值转成本组件预期的数据类型
     *
     * @param field 关联字段
     * @param val   页面传值
     * @return 此组件预期的代表传值的java类型对象
     */
    Object getValue(PageFieldDto field, Object val);

    /**
     * 级联新增
     *
     * @param field 关联字段
     * @param bizId 主表的主键
     * @param comp  此字段的页面传值
     */
    void create(PageFieldDto field, String bizId, Object comp);

    /**
     * 级联更新
     *
     * @param field 关联字段
     * @param bizId 主表的主键
     * @param comp  此字段的页面传值
     */
    void update(PageFieldDto field, String bizId, Object comp);

    /**
     * 级联删除
     *
     * @param field 关联字段
     * @param bizId 主数据主键
     */
    void remove(PageFieldDto field, List<String> bizId);

    /**
     * 根据关联的业务表主键，获取其关联的数据
     * <p>
     * 注意：此方法会在每次查询后自动调用，如果使用不当将会影响性能
     *
     * @param field 当前字段
     * @param bizId 主数据业务主键
     * @return Map<驼峰命名的fieldName, value>
     */
    Map<String, Object> getForForm(PageFieldDto field, String bizId);

    /**
     * 列表页面获取此组件数据的接口
     *
     * @param field     当前字段
     * @param bizIdList 列表上主数据业务主键
     * @return Map<bizId, Map < 驼峰命名的fieldName, value>>
     */
    Map<String, Map<String, Object>> getForList(PageFieldDto field, List<String> bizIdList);

    /**
     * 发布
     *
     * @param field 关联字段
     */
    void onPublish(PageFieldDto field);

}
