package com.nway.platform.wform.lib.design.entity;

import com.nway.platform.wform.lib.commons.nway_jdbc.LocalDateTimeNow;
import com.nway.platform.wform.lib.commons.nway_jdbc.LocalDateTimeNowOnInsert;
import com.nway.spring.jdbc.annotation.Column;
import com.nway.spring.jdbc.sql.LogicFieldStrategy;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BaseEntity {
    @Column(fillStrategy = LocalDateTimeNowOnInsert.class)
    private LocalDateTime createTime;
    @Column(fillStrategy = LocalDateTimeNow.class)
    private LocalDateTime updateTime;
    @Column(fillStrategy = LogicFieldStrategy.class, permissionStrategy = LogicFieldStrategy.class)
    private Boolean isDeleted;
}
