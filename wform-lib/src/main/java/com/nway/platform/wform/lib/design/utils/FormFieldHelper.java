package com.nway.platform.wform.lib.design.utils;

import com.nway.platform.wform.lib.components.MultiValueComponent;
import com.nway.platform.wform.lib.components.SingleValueComponent;
import com.nway.platform.wform.lib.design.dto.PageFieldDto;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FormFieldHelper {

    public static List<PageFieldDto> getSimpleValueFormFields(List<PageFieldDto> formFields) {

        List<PageFieldDto> baseTypeFormFields = new ArrayList<>();
        for (PageFieldDto form : formFields) {
            if (CollectionUtils.contains(Arrays.asList(form.getComponentObj().getClass().getInterfaces()).iterator(), SingleValueComponent.class)) {
                baseTypeFormFields.add(form);
            }
        }

        return baseTypeFormFields;
    }

    public static List<PageFieldDto> getMultiValueFormFields(List<PageFieldDto> formFields) {

        return formFields.stream().filter(field -> field.getComponentObj() instanceof MultiValueComponent).collect(Collectors.toList());
    }
}
