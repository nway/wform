package com.nway.platform.wform.lib.commons.utils;

import com.nway.platform.wform.lib.exception.WFormException;
import com.nway.spring.jdbc.pagination.Page;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

public class BeanUtils {

    public static void copy(Object source, Object target) {
        org.springframework.beans.BeanUtils.copyProperties(source, target);
    }

    public static <T> T copy(Object source, Class<T> target) {
        T instance = getInstance(target);
        org.springframework.beans.BeanUtils.copyProperties(source, instance);
        return instance;
    }

    public static <T> List<T> copyToList(List<?> source, Class<T> target) {
        return source.stream().map(e -> {
            T instance = getInstance(target);
            org.springframework.beans.BeanUtils.copyProperties(e, instance);
            return instance;
        }).collect(Collectors.toList());
    }

    public static <T> Page<T> copyToPage(Page<?> source, Class<T> target) {
        Page<T> page = new Page<>();
        page.setCurrentPage(source.getCurrentPage());
        page.setPageCount(source.getPageCount());
        page.setPageSize(source.getPageSize());
        page.setTotal(source.getTotal());
        List<T> data = copyToList(source.getPageData(), target);
        page.setPageData(data);
        return page;
    }

    private static <T> T getInstance(Class<T> target) {
        T t;
        try {
            t = target.getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                 NoSuchMethodException e) {
            throw new WFormException("对象转换失败", e);
        }
        return t;
    }
}
