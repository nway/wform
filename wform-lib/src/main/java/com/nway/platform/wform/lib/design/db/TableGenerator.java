package com.nway.platform.wform.lib.design.db;

import com.nway.platform.wform.lib.components.SingleValueComponent;
import com.nway.platform.wform.lib.design.dto.BasePage;
import com.nway.platform.wform.lib.design.dto.PageFieldDto;
import com.nway.platform.wform.lib.design.utils.FormFieldHelper;
import com.nway.spring.jdbc.SqlExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Component
public class TableGenerator {

    @Autowired
    private SqlExecutor sqlExecutor;

    public void createTable(BasePage page) {

        JdbcTemplate jdbcTemplate = sqlExecutor.getJdbcTemplate();

        String mainTableSql = makeMainTableCreateSql(page);
        jdbcTemplate.execute(mainTableSql);

    }

    private String makeMainTableCreateSql(BasePage page) {

        StringBuilder mainTableSql = new StringBuilder("create table ");

        mainTableSql.append(page.getTableName()).append(" ( ");

        List<PageFieldDto> form = FormFieldHelper.getSimpleValueFormFields(page.getFields());

        for (PageFieldDto field : form) {
            SingleValueComponent component = (SingleValueComponent) field.getComponentObj();
            mainTableSql
                    .append(field.getName()).append(' ')
                    .append(component.getSqlType(Optional.ofNullable(field.getSize()).orElse("255")))
                    .append(" COMMENT '").append(field.getDisplay()).append("',");
        }

        return !form.isEmpty() ? mainTableSql.deleteCharAt(mainTableSql.length() - 1).append(')').toString() : "";
    }

}
