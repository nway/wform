package com.nway.platform.wform.lib.design.dto;

import com.nway.spring.jdbc.annotation.Table;
import lombok.Data;

/**
 * 页面字段的定义，与页面无关的基础信息
 */
@Data
@Table("t_form_field_attr")
public class FormFieldAttrDto {

    private String id;

    private String fieldId;

    private String attrName;

    private String attrValue;

}
