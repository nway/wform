package com.nway.platform.wform.lib.exception;

public class WFormException extends RuntimeException {

    public WFormException() {
        super();
    }

    public WFormException(String message) {
        super(message);
    }

    public WFormException(String code, String message) {
        super(message);
    }

    public WFormException(Throwable cause) {
        super(cause);
    }

    public WFormException(String message, Throwable cause) {
        super(message, cause);
    }

}
