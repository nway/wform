package com.nway.platform.wform.lib.design.dto;

import com.nway.platform.wform.lib.access.validator.Validator;
import com.nway.platform.wform.lib.components.BaseComponent;
import lombok.Data;

import java.util.Map;

/**
 * 页面字段的定义，与页面无关的基础信息
 */
@Data
public class PageFieldDto {

    private String id;

    private String fieldId;

    private String ownerId;

    /**
     * 用做页面元素 id 或 name，也是页面给后台传递数据时的属性名
     */
    private String name;

    private String nameCamel;

    private String nameUnder;

    /**
     * 中文名称，当需要根据后台定义生成页面时，此字段为页面字段中文名
     */
    private String display;

    private String validator;

    private transient Validator validatorObj;

    private String size;

    /**
     * 组件类别
     */
    private String component;

    private transient BaseComponent componentObj;

    private Boolean isMultiValue;

    private Map<String, String> fieldAttr;

    /**
     * 是否为权限字段，权限字段会在增删改查所有数据操作时自动补入sql
     */
    private Boolean isAuthorityField;
    /**
     * 表单数据显示时是否隐藏，暂不作为现在列表数据的标识，列表字段手动选择确定，被选字段都应显示
     */
    private Boolean isHidden;

    @Override
    public String toString() {
        return "name=" + name + " component=" + component;
    }
}
