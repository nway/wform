package com.nway.platform.wform.lib.components;

import com.nway.platform.wform.lib.design.dto.PageFieldDto;

public interface Initializable {

    /**
     * 初始化数据
     *
     * @return
     */
    Object init(PageFieldDto field);
}
