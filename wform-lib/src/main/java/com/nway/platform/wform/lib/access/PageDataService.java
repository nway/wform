package com.nway.platform.wform.lib.access;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.nway.platform.wform.lib.access.handler.HandlerType;
import com.nway.platform.wform.lib.access.handler.PageDataHandler;
import com.nway.platform.wform.lib.access.sql.InsertBuilder;
import com.nway.platform.wform.lib.access.sql.UpdateBuilder;
import com.nway.platform.wform.lib.commons.SpringContextUtils;
import com.nway.platform.wform.lib.commons.constant.SearchTypeConstant;
import com.nway.platform.wform.lib.components.AuthorizableComponent;
import com.nway.platform.wform.lib.components.MultiValueComponent;
import com.nway.platform.wform.lib.components.SingleValueComponent;
import com.nway.platform.wform.lib.design.dto.*;
import com.nway.platform.wform.lib.design.service.PageService;
import com.nway.platform.wform.lib.design.utils.StringUtils;
import com.nway.platform.wform.lib.exception.WFormException;
import com.nway.spring.jdbc.SqlExecutor;
import com.nway.spring.jdbc.bean.processor.ColumnMapRowMapper;
import com.nway.spring.jdbc.pagination.Page;
import com.nway.spring.jdbc.sql.SQL;
import com.nway.spring.jdbc.sql.builder.QueryBuilder;
import io.seata.common.exception.ShouldNeverHappenException;
import io.seata.tm.api.DefaultFailureHandlerImpl;
import io.seata.tm.api.FailureHandler;
import io.seata.tm.api.TransactionalExecutor;
import io.seata.tm.api.TransactionalTemplate;
import io.seata.tm.api.transaction.Propagation;
import io.seata.tm.api.transaction.RollbackRule;
import io.seata.tm.api.transaction.TransactionInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlTypeValue;
import org.springframework.jdbc.core.StatementCreatorUtils;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.client.ResourceAccessException;

import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * 页面数据处理服务类
 */
@Slf4j
@Service
public class PageDataService {

    @Autowired
    private SqlExecutor sqlExecutor;
    @Autowired
    private PageService pageService;
    @Autowired
    private DataSourceTransactionManager txManager;

    private static final ColumnMapRowMapper COLUMN_MAP_ROW_MAPPER = new ColumnMapRowMapper();

    public Object save(String pageName, Map<String, Object> formData) {

        Object retVal = new Object();
        FormPageDto page = (FormPageDto) pageService.getByName(pageName);

        if (Boolean.FALSE.equals(page.getEditable())) {
            throw new UnsupportedOperationException("此页面不支持保存数据");
        }

        Object pkVal = formData.get(page.getKeyField().getNameCamel());

        List<String> errMsg = new ArrayList<>();
        for (PageFieldDto field : page.getFields()) {
            String msg = field.getValidatorObj().validate(field.getDisplay(), formData.get(field.getNameCamel()));
            if (StringUtils.isNotBlank(msg)) {
                errMsg.add(msg);
            }
        }
        if (CollectionUtils.isNotEmpty(errMsg)) {
            throw new WFormException(Joiner.on('，').join(errMsg));
        }
        for (PageFieldDto field : page.getFields()) {
            if (!field.getIsMultiValue()) {
                formData.put(field.getNameCamel(), ((SingleValueComponent) field.getComponentObj()).getValue(formData.get(field.getNameCamel())));
            }
        }

        // 主键有值  表示是更新操作
        boolean isUpdate = pkVal != null && !pkVal.toString().trim().isEmpty() && !Boolean.TRUE.equals(formData.get("WFORM_FORCE_INSERT_INNER"));
        if (isUpdate) {
            if (Optional.ofNullable(page.getGtxUpdate()).orElse(Boolean.FALSE)) {
                retVal = new GlobalTX() {
                    @Override
                    Object execute() throws Throwable {
                        return doGlobalTxUpdate(() -> doUpdate(page, formData));
                    }
                }.doExecute();
            } else {
                retVal = doLocalTxUpdate(() -> doUpdate(page, formData));
            }
        } // 新增
        else {
            if (Optional.ofNullable(page.getGtxCreate()).orElse(Boolean.FALSE)) {
                retVal = new GlobalTX() {
                    @Override
                    Object execute() throws Throwable {
                        return doGlobalTxCreate(() -> doCreate(page, formData));
                    }
                }.doExecute();
            } else {
                retVal = doLocalTxCreate(() -> doCreate(page, formData));
            }
        }
        return retVal;
    }

    private Object doGlobalTxCreate(Supplier<Object> supplier) throws Throwable {
        return new TransactionalTemplate().execute(new TransactionalExecutor() {
            @Override
            public Object execute() throws Throwable {
                log.debug("基于全局事务执行新增数据操作");
                return supplier.get();
            }

            @Override
            public TransactionInfo getTransactionInfo() {
                return PageDataService.this.getTransactionInfo("doGlobalTxCreate(com.nway.platform.wform.lib.design.dto.PageDto, java.util.Map)");
            }
        });
    }

    private Object doLocalTxCreate(Supplier<Object> supplier) {

        log.debug("基于本地事务执行新增数据操作");
        TransactionStatus transactionStatus = getTransaction();
        try {
            Object val = supplier.get();
            txManager.commit(transactionStatus);
            return val;
        } catch (Throwable e) {
            txManager.rollback(transactionStatus);
            throw e;
        }
    }

    /**
     * FormPage.PAGE_TYPE_CREATE && group.isEditable()
     *
     * @param page
     * @param formData
     */
    private Object doCreate(FormPageDto page, Map<String, Object> formData) {

        PageDataHandler formPageDataHandler = getPageDataHandler(page.getName());

        formPageDataHandler.handleParam(HandlerType.DATA_CREATE, page, formData);

        Map<String, Object> simpleData = new HashMap<>();
        for (PageFieldDto field : page.getFields()) {
            if (field.getComponentObj() instanceof SingleValueComponent) {
                simpleData.put(field.getNameUnder(), formData.get(field.getNameCamel()));
            }
        }
        int effectCount = sqlExecutor.update(new InsertBuilder(pageService.getBeanClass(page), page.getTableName()).use(simpleData));

        if (effectCount > 0) {
            for (PageFieldDto field : page.getFields()) {
                if (Optional.ofNullable(field.getIsMultiValue()).orElse(Boolean.FALSE)) {
                    ((MultiValueComponent) field.getComponentObj()).create(field,
                            formData.get(page.getKeyField().getNameCamel()).toString(), formData.get(field.getNameCamel()));
                }
            }
        }

        formPageDataHandler.handleResult(HandlerType.DATA_CREATE, page, formData);

        return formData.get(page.getKeyField().getNameCamel());
    }

    private Object doGlobalTxUpdate(Supplier<Object> supplier) throws Throwable {
        return new TransactionalTemplate().execute(new TransactionalExecutor() {
            @Override
            public Object execute() throws Throwable {
                log.debug("基于全局事务执行更新数据操作");
                return supplier.get();
            }

            @Override
            public TransactionInfo getTransactionInfo() {
                return PageDataService.this.getTransactionInfo("doGlobalTxUpdate(com.nway.platform.wform.lib.design.dto.PageDto, java.util.Map)");
            }
        });
    }

    private Object doLocalTxUpdate(Supplier<Object> supplier) {
        log.debug("基于本地事务执行更新数据操作");
        TransactionStatus transactionStatus = getTransaction();
        try {
            Object val = supplier.get();
            txManager.commit(transactionStatus);
            return val;
        } catch (Throwable e) {
            txManager.rollback(transactionStatus);
            throw e;
        }
    }

    /**
     * FormPage.PAGE_TYPE_EDIT && group.isEditable()
     *
     * @param page
     * @param formData
     */
    private Object doUpdate(FormPageDto page, Map<String, Object> formData) {

        PageDataHandler pageDataHandler = getPageDataHandler(page.getName());

        pageDataHandler.handleParam(HandlerType.DATA_MODIFY, page, formData);

        Map<String, Object> simpleData = new HashMap<>();
        for (PageFieldDto field : page.getFields()) {
            if (field.getComponentObj() instanceof SingleValueComponent) {
                simpleData.put(field.getNameUnder(), formData.get(field.getNameCamel()));
            }
        }
        int effectCount = sqlExecutor.update(new UpdateBuilder(pageService.getBeanClass(page), page.getTableName()).use(simpleData).eq(page.getKeyField().getNameUnder(), simpleData.get(page.getKeyField().getNameUnder())));

        log.debug("数据更新完成,受影响行数:{}行", effectCount);

        String keyName = page.getKeyField().getNameCamel();

        if (effectCount > 0) {
            for (PageFieldDto field : page.getFields()) {
                // 子表操作  当字段类型为多值类型并且参数中包含时才考虑执行关联操作
                if (Optional.ofNullable(field.getIsMultiValue()).orElse(Boolean.FALSE) && formData.containsKey(field.getNameCamel())) {
                    log.debug("更新数据时发现多值字段,组件类型:{},字段id:{},业务主键:{},此组件数据：{}", field.getComponent(), field.getId(), formData.get(keyName), formData.get(field.getNameCamel()));
                    ((MultiValueComponent) field.getComponentObj()).update(field,
                            formData.get(keyName).toString(), formData.get(field.getNameCamel()));
                }
            }
        }

        pageDataHandler.handleResult(HandlerType.DATA_MODIFY, page, formData);

        return formData.get(keyName);
    }

    /**
     * 对应详情页面
     *
     * @param pageName
     * @param dataId
     * @return
     */
    public Map<String, Object> get(String pageName, String dataId) {

        BasePage page = pageService.getByName(pageName);
        String keyName = page.getKeyField().getNameCamel();

        Map<String, Object> param = new HashMap<>();
        param.put(keyName, dataId);

        PageDataHandler pageDataHandler = getPageDataHandler(page.getName());
        pageDataHandler.handleParam(HandlerType.DATA_DETAILS, page, param);

        QueryBuilder queryBuilder = SQL.query(pageService.getBeanClass(page))
                // 单值字段名
                .withColumn(page.getFields().stream()
                        .filter(fieldDto -> !fieldDto.getIsMultiValue() && !fieldDto.getIsHidden() && !fieldDto.getIsAuthorityField())
                        .map(PageFieldDto::getNameUnder).collect(Collectors.joining(",")))
                .eq(page.getKeyField().getNameUnder(), dataId);

        page.getFields().stream()
                .filter(field -> field.getIsAuthorityField() && field.getComponentObj() instanceof AuthorizableComponent).forEach(field -> {
                    Object searchVal = ((AuthorizableComponent) field.getComponentObj()).getSearchVal();
                    if (searchVal instanceof Collection) {
                        queryBuilder.in(field.getNameUnder(), (Collection<?>) searchVal);
                    } else {
                        queryBuilder.eq(field.getNameUnder(), searchVal);
                    }
                });

        String sql = queryBuilder.getSql();
        Object[] params = queryBuilder.getParam().toArray();

        if (log.isDebugEnabled()) {
            log.debug("sql = {}", sql);
            log.debug("params = {} ", Arrays.deepToString(params));
        }

        Map<String, Object> pageData = sqlExecutor.getJdbcTemplate().queryForObject(sql, params, getSqlType(params), COLUMN_MAP_ROW_MAPPER);

        if (pageData == null || pageData.isEmpty()) {
            throw new ResourceAccessException("数据不存在");
        }

        for (PageFieldDto field : page.getFields()) {
            // 子表操作
            if (Optional.ofNullable(field.getIsMultiValue()).orElse(Boolean.FALSE)) {
                String bizId = pageData.get(keyName).toString();
                log.debug("获取数据详情时发现多值字段,组件类型:{},字段id:{},业务主键{}", field.getComponent(), field.getId(), bizId);
                pageData.put(field.getNameCamel(), ((MultiValueComponent) field.getComponentObj()).getForForm(field, bizId));
            }
        }

        pageDataHandler.handleResult(HandlerType.DATA_DETAILS, page, pageData);

        return pageData;
    }

    public List<Map<String, Object>> queryList(String pageName, Map<String, Object> param) {

        ListPageDto page = (ListPageDto) pageService.getByName(pageName);
        Map<String, Object> queryParam = prepareQueryListParam(page, param);

        if (queryParam == null) {
            return Lists.newArrayList();
        }

        PageDataHandler pageDataHandler = getPageDataHandler(page.getName());
        pageDataHandler.handleParam(HandlerType.DATA_LIST, page, queryParam);

        List<PageFieldDto> authorityFields = pageService.getAuthorityField(pageName);

        QueryBuilder queryBuilder = SQL.query(pageService.getBeanClass(page));
        fillCondition(page, authorityFields, queryParam, queryBuilder);
        Object[] params = queryBuilder.getParam().toArray();

        List<Map<String, Object>> pageData = sqlExecutor.getJdbcTemplate().query(queryBuilder.getSql(), params, getSqlType(params), COLUMN_MAP_ROW_MAPPER);

        fillList(page, pageData);

        // 列表页面数据后处理入口
        pageDataHandler.handleResult(HandlerType.DATA_LIST, page, pageData);

        return pageData;
    }

    public void export(String pageName, Map<String, Object> param, OutputStream outputStream) {

        BasePage page = pageService.getByName(pageName);

        List<PageFieldDto> pageListFields = page.getFields();
        List<Map<String, Object>> dataList = queryList(pageName, param);

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();

        int pkFlag = 0;
        XSSFRow firstRow = sheet.createRow(0);
        for (int i = 0; i < pageListFields.size(); i++) {
            PageFieldDto pageFieldDto = pageListFields.get(i);
            // 主键不导出
            if (pageFieldDto == page.getKeyField()) {
                pkFlag = 1;
                continue;
            }
            XSSFCell cell = firstRow.createCell(i - pkFlag);
            cell.setCellValue(pageFieldDto.getDisplay());
        }

        for (int i = 0; i < dataList.size(); i++) {
            pkFlag = 0;
            XSSFRow dataRow = sheet.createRow(i + 1);
            for (int n = 0; n < pageListFields.size(); n++) {
                PageFieldDto pageFieldDto = pageListFields.get(n);
                // 主键不导出
                if (pageFieldDto == page.getKeyField()) {
                    pkFlag = 1;
                    continue;
                }
                XSSFCell cell = dataRow.createCell(n - pkFlag);
                Object data = dataList.get(i).get(pageFieldDto.getNameCamel());
                if (data instanceof Number) {
                    cell.setCellValue(Double.parseDouble(data.toString()));
                } else if (data instanceof Date) {
                    cell.setCellValue((Date) data);
                } else if (data instanceof LocalDate) {
                    cell.setCellValue((LocalDate) data);
                } else if (data instanceof LocalDateTime) {
                    cell.setCellValue((LocalDateTime) data);
                } else {
                    cell.setCellValue(String.valueOf(data));
                }
            }
        }
        try {
            workbook.write(outputStream);
            workbook.close();
        } catch (IOException e) {
            throw new WFormException("数据导出失败", e);
        }
    }

    /**
     * 对应 FormPage.PAGE_TYPE_LIST
     * <p>
     * 对于点击列表在本页显示详情的情况，使用详情页中加list组件解决
     *
     * @param pageName
     * @param param
     * @return
     */
    public Page<Map<String, Object>> queryPage(String pageName, Map<String, Object> param) {

        int pageNum = NumberUtils.toInt((String) param.get("pageNum"), 1);
        int pageSize = NumberUtils.toInt((String) param.get("pageSize"), 15);

        Page<Map<String, Object>> retVal = new Page<>(pageSize, pageNum);

        ListPageDto page = (ListPageDto) pageService.getByName(pageName);
        Map<String, Object> queryParam = prepareQueryListParam(page, param);

        if (queryParam == null || queryParam.isEmpty()) {
            return retVal;
        }

        PageDataHandler pageDataHandler = getPageDataHandler(page.getName());
        pageDataHandler.handleParam(HandlerType.DATA_LIST, page, queryParam);

        List<PageFieldDto> authorityFields = pageService.getAuthorityField(pageName);

        QueryBuilder queryBuilder = SQL.query(pageService.getBeanClass(page));
        fillCondition(page, authorityFields, queryParam, queryBuilder);
        Object[] params = queryBuilder.getParam().toArray();

        Page<Map<String, Object>> pageData = sqlExecutor.queryPage(queryBuilder.getSql(), params, pageNum, pageSize);

        fillList(page, pageData.getPageData());

        // 列表页面数据后处理入口
        pageDataHandler.handleResult(HandlerType.DATA_LIST, page, pageData.getPageData());

        retVal.setPageData(pageData.getPageData());
        retVal.setPageCount(pageData.getPageCount());
        retVal.setTotal(pageData.getTotal());

        return retVal;
    }

    private void fillList(ListPageDto page, List<Map<String, Object>> pageData) {

        if (CollectionUtils.isEmpty(pageData)) {
            return;
        }

        // 多值字段
        Set<PageFieldDto> multiValueFieldSet = page.getFields().stream().filter(field -> Boolean.TRUE.equals(field.getIsMultiValue())).collect(Collectors.toSet());

        if (multiValueFieldSet.isEmpty()) {
            return;
        }

        String keyName = page.getKeyField().getNameCamel();
        // 获取多值字段所有主键值
        Map<String, List<String>> multiValueBizIdMap = new HashMap<>();
        Set<String> fieldNameSet = multiValueFieldSet.stream().map(PageFieldDto::getNameCamel).collect(Collectors.toSet());

        for (String fieldName : fieldNameSet) {
            for (Map<String, Object> row : pageData) {
                List<String> bizIdList = multiValueBizIdMap.get(fieldName);
                if (bizIdList == null) {
                    bizIdList = new ArrayList<>();
                    bizIdList.add(String.valueOf(row.get(keyName)));
                    multiValueBizIdMap.put(fieldName, bizIdList);
                } else {
                    bizIdList.add(String.valueOf(row.get(keyName)));
                }
            }
        }

        // 获取多值字段所有数据的关联数据
        Map<String, Map<String, Map<String, Object>>> multiValueDataMp = new HashMap<>();
        for (PageFieldDto field : multiValueFieldSet) {
            List<String> bizIds = multiValueBizIdMap.get(field.getNameCamel());
            if (CollectionUtils.isEmpty(bizIds)) {
                continue;
            }
            Map<String, Map<String, Object>> associatedValue = ((MultiValueComponent) field.getComponentObj()).getForList(field, bizIds);
            multiValueDataMp.put(field.getNameCamel(), associatedValue);
        }

        // 将多值字段的关联数据设置到对应行数据内  根据主键值
        for (String fieldName : fieldNameSet) {
            for (Map<String, Object> data : pageData) {
                Map<String, Map<String, Object>> oneFieldDataMap = multiValueDataMp.get(fieldName);
                if (oneFieldDataMap != null) {
                    data.putAll(oneFieldDataMap.getOrDefault(String.valueOf(data.get(keyName)), new HashMap<>()));
                }
            }
        }
    }

    private Map<String, Object> prepareQueryListParam(ListPageDto page, Map<String, Object> param) {

        Map<String, Object> queryParam = new HashMap<>();

        // 多指组件有参数值
        for (PageFieldDto field : page.getConditionField()) {
            if (field.getIsMultiValue()) {
                queryParam.put(field.getNameCamel(), ((MultiValueComponent) field.getComponentObj()).getParentId(field, param.get(field.getNameCamel())));
            } else {
                queryParam.put(field.getNameCamel(), ((SingleValueComponent) field.getComponentObj()).getValue(param.get(field.getNameCamel())));
            }
        }

        // 判断是否有
        boolean hasMultiValParam = page.getConditionField().stream()
                .filter(field -> Optional.ofNullable(field.getIsMultiValue()).orElse(Boolean.FALSE))
                .map(e -> param.getOrDefault(e.getNameCamel(), "").toString())
                .anyMatch(StringUtils::isNotBlank);
        if (hasMultiValParam) {
            // 子表关联的主表的主键
            List<Collection<Object>> bizIdAllList = page.getConditionField().stream().filter(field -> Optional.ofNullable(field.getIsMultiValue()).orElse(Boolean.FALSE))
                    .map(field -> {
                        Object objVal = queryParam.get(field.getNameCamel());
                        if (objVal instanceof Collection) {
                            return (Collection<Object>) objVal;
                        }
                        return Lists.newArrayList(objVal);
                    }).collect(Collectors.toList());

            // 从所有多值组件获取到的业务id为空 并且此查询有多值组件  则说明没有符合条件的数据  则不必继续执行
            if (bizIdAllList.isEmpty() || bizIdAllList.stream().allMatch(CollectionUtils::isEmpty)) {
                return null;
            }

            // 所有关联数据的交集
            Collection<Object> intersection = bizIdAllList.get(0);
            for (int i = 1; i < bizIdAllList.size(); i++) {
                intersection = CollectionUtils.intersection(intersection, bizIdAllList.get(i));
            }
            Collection<Object> relBizIdList = intersection.stream().filter(Objects::nonNull).distinct()
                    .map(bizId -> ((SingleValueComponent) page.getKeyField().getComponentObj()).getValue(bizId))
                    .collect(Collectors.toList());
            queryParam.put(page.getKeyField().getNameCamel(), relBizIdList);
        }
        return queryParam;
    }

    public void remove(String pageName, List<String> dataId) {
        FormPageDto page = (FormPageDto) pageService.getByName(pageName);
        if (Optional.ofNullable(page.getGtxDelete()).orElse(Boolean.FALSE)) {
            new GlobalTX() {
                @Override
                Object execute() throws Throwable {
                    doGlobalTxRemove(page, dataId);
                    return null;
                }
            }.doExecute();
        } else {
            doLocalTxRemove(page, dataId);
        }
    }

    private void doGlobalTxRemove(FormPageDto page, List<String> dataId) throws Throwable {
        new TransactionalTemplate().execute(new TransactionalExecutor() {
            @Override
            public Object execute() throws Throwable {
                log.debug("基于全局事务执行数据删除操作，pageId:{}, bizId:{}", page.getId(), dataId);
                doRemove(page, dataId);
                return null;
            }

            @Override
            public TransactionInfo getTransactionInfo() {
                return PageDataService.this.getTransactionInfo("doGlobalTxRemove(com.nway.platform.wform.lib.design.dto.PageDto, java.lang.String)");
            }
        });
    }

    private void doLocalTxRemove(FormPageDto page, List<String> dataId) {

        log.debug("基于本地事务执行数据删除操作，pageId:{}, bizId:{}", page.getId(), dataId);

        TransactionStatus transactionStatus = getTransaction();
        try {
            doRemove(page, dataId);
        } catch (Throwable e) {
            txManager.rollback(transactionStatus);
            throw e;
        }
        txManager.commit(transactionStatus);
    }

    private void doRemove(FormPageDto page, List<String> dataIds) {

        PageDataHandler pageDataHandler = getPageDataHandler(page.getName());

        Map<String, Object> param = Collections.singletonMap("bizId", dataIds);

        pageDataHandler.handleParam(HandlerType.DATA_REMOVE, page, param);

        int effectCount = sqlExecutor.batchDeleteById(dataIds, pageService.getBeanClass(page));

        if (effectCount > 0) {
            page.getFields().stream()
                    .filter(field -> field.getComponentObj() instanceof MultiValueComponent)
                    .forEach(field -> ((MultiValueComponent) field.getComponentObj()).remove(field, dataIds));
        }

        pageDataHandler.handleResult(HandlerType.DATA_REMOVE, page, param);
    }

    private TransactionInfo getTransactionInfo(String txName) {
        TransactionInfo transactionInfo = new TransactionInfo();
        transactionInfo.setTimeOut(60000);
        transactionInfo.setName(txName);
        transactionInfo.setPropagation(Propagation.REQUIRED);
        transactionInfo.setRollbackRules(Sets.newHashSet(new RollbackRule(Exception.class)));
        return transactionInfo;
    }

    private TransactionStatus getTransaction() {
        // 建立事务的定义
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        return txManager.getTransaction(def);
    }

    private void fillCondition(ListPageDto page, List<PageFieldDto> authorityFields, Map<String, Object> params, QueryBuilder sqlBuilder) {
        sqlBuilder.withColumn(page.getFields().stream().filter(fieldDto -> !fieldDto.getIsMultiValue()).map(PageFieldDto::getNameUnder).collect(Collectors.joining(",")));
        for (ListPageFieldDto field : page.getConditionField()) {
            Object param = params.get(field.getNameCamel());
            if (param != null && !field.getIsMultiValue()) {
                if (param instanceof Collection && CollectionUtils.isNotEmpty((Collection<?>) param)) {
                    sqlBuilder.in(field.getNameUnder(), (Collection<?>) param);
                } else if (SearchTypeConstant.eq.equals(field.getSearchType())) {
                    sqlBuilder.eq(field.getNameUnder(), param);
                } else if (SearchTypeConstant.like.equals(field.getSearchType())) {
                    sqlBuilder.like(field.getNameUnder(), param.toString());
                } else if (SearchTypeConstant.rightLike.equals(field.getSearchType())) {
                    sqlBuilder.likeRight(field.getNameUnder(), param.toString());
                }
            }
        }
        Optional.ofNullable(authorityFields).orElse(Collections.emptyList())
                .stream().filter(field -> field.getComponentObj() instanceof AuthorizableComponent)
                .forEach(authorityField -> {
                    Object searchVal = ((AuthorizableComponent) authorityField.getComponentObj()).getSearchVal();
                    if (searchVal instanceof Collection) {
                        sqlBuilder.in(authorityField.getNameUnder(), (Collection<?>) searchVal);
                    } else {
                        sqlBuilder.eq(authorityField.getNameUnder(), searchVal);
                    }
                });

    }

    private static abstract class GlobalTX {

        abstract Object execute() throws Throwable;

        public Object doExecute() {
            try {
                return execute();
            } catch (TransactionalExecutor.ExecutionException e) {
                TransactionalExecutor.Code code = e.getCode();
                FailureHandler failureHandler = new DefaultFailureHandlerImpl();
                switch (code) {
                    case RollbackDone:
                        throw new WFormException(e.getOriginalException());
                    case BeginFailure:
                        failureHandler.onBeginFailure(e.getTransaction(), e.getCause());
                        throw new WFormException(e.getCause());
                    case CommitFailure:
                        failureHandler.onCommitFailure(e.getTransaction(), e.getCause());
                        throw new WFormException(e.getCause());
                    case RollbackFailure:
                        failureHandler.onRollbackFailure(e.getTransaction(), e.getOriginalException());
                        throw new WFormException(e.getOriginalException());
                    case RollbackRetrying:
                        failureHandler.onRollbackRetrying(e.getTransaction(), e.getOriginalException());
                        throw new WFormException(e.getOriginalException());
                    default:
                        throw new ShouldNeverHappenException(String.format("Unknown TransactionalExecutor.Code: %s", code));
                }
            } catch (Throwable throwable) {
                throw new WFormException(throwable);
            }
        }
    }

    private static final PageDataHandler defaultFormPageDataHandler = new DefaultPageDataHandler();

    private static final class DefaultPageDataHandler implements PageDataHandler {

        @Override
        public void handleParam(HandlerType handlerType, BasePage formPage, Map<String, Object> param) {

        }

        @Override
        public void handleResult(HandlerType handlerType, BasePage page, Object data) {

        }
    }

    private PageDataHandler getPageDataHandler(String pageName) {

        return SpringContextUtils.getBean(pageName + "DataHandler", PageDataHandler.class, defaultFormPageDataHandler);
    }

    private int[] getSqlType(Object[] objs) {
        if (objs == null) {
            return null;
        }
        if (objs.length == 0) {
            return new int[0];
        }
        return Arrays.stream(objs)
                .map(obj -> {
                    if (obj == null) {
                        return SqlTypeValue.TYPE_UNKNOWN;
                    }
                    return StatementCreatorUtils.javaTypeToSqlParameterType(obj.getClass());
                })
                .mapToInt(x -> x)
                .toArray();
    }
}
