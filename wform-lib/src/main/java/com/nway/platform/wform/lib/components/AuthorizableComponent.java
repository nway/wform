package com.nway.platform.wform.lib.components;

public interface AuthorizableComponent {

    /**
     * 通常与当前登陆人相关，无需参数传入。
     *
     * @return 数据类型应为数值、字符串，或存储前述类型数据的集合。
     */
    Object getSearchVal();
}
