package com.nway.platform.wform.lib.design.dao;

import com.nway.platform.wform.lib.design.entity.Module;
import com.nway.spring.jdbc.SqlExecutor;
import com.nway.spring.jdbc.sql.SQL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ModuleDao {

    @Autowired
    private SqlExecutor sqlExecutor;

    public String getNameById(String id) {
        Module module = sqlExecutor.queryFirst(SQL.query(Module.class).withColumn(Module::getName).eq(Module::getId, id));
        return module.getName();
    }

    public List<Module> listAll() {
        return sqlExecutor.queryList(SQL.query(Module.class).orderBy(Module::getName));
    }
}
