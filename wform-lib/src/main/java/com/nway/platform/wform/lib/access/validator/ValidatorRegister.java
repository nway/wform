package com.nway.platform.wform.lib.access.validator;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ValidatorRegister implements InitializingBean, ApplicationContextAware {

    private ApplicationContext applicationContext;

    private Map<String, Validator> components = new HashMap<>();

    public Validator getValidator(String name) {

        return components.get(name + "Validator");
    }

    @Override
    public void afterPropertiesSet() throws Exception {

        this.components = applicationContext.getBeansOfType(Validator.class, true, false);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

        this.applicationContext = applicationContext;
    }

}
