package com.nway.platform.wform.lib.design.service;

import com.nway.platform.wform.lib.design.dao.ModuleDao;
import com.nway.platform.wform.lib.design.entity.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModuleService {

    @Autowired
    private ModuleDao moduleDao;

    public List<Module> listAll() {
        return moduleDao.listAll();
    }
}
