package com.nway.platform.wform.lib.design.dto;

import lombok.Data;

@Data
public class PageNodeDto {

    private String id;

    private String title;

    private String parentId;

    private String type;
}
