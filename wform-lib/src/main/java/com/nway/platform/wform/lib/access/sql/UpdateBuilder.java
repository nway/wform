package com.nway.platform.wform.lib.access.sql;

import com.nway.spring.jdbc.sql.SqlBuilderUtils;
import com.nway.spring.jdbc.sql.SqlType;
import com.nway.spring.jdbc.sql.builder.SqlBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UpdateBuilder extends SqlBuilder<UpdateBuilder> {

    private final String tableName;
    private final List<String> sets = new ArrayList<>();

    public UpdateBuilder(Class<?> beanClass, String tableName) {
        super(beanClass);
        this.tableName = tableName;
    }

    public UpdateBuilder use(Map<String, Object> data) {
        String key = SqlBuilderUtils.getIdName(beanClass);
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            if (key.equals(entry.getKey())) {
                continue;
            }
            if (entry.getValue() != null && !isInvalid(entry.getValue())) {
                sets.add(entry.getKey() + " = ?");
                param.add(entry.getValue());
            }
        }
        return this;
    }

    @Override
    protected SqlType getSqlType() {
        return SqlType.UPDATE;
    }

    @Override
    public String getSql() {
        return "update " + this.tableName + " set " + String.join(",", sets) + super.getSql();
    }

}
