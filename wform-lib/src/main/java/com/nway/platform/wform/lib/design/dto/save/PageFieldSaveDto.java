package com.nway.platform.wform.lib.design.dto.save;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class PageFieldSaveDto {

    private String id;

    private String fieldId;

    /**
     * 用做页面元素 id 或 name，也是页面给后台传递数据时的属性名
     */
    private String name;

    /**
     * 原来的字段名
     */
    private String oldName;

    /**
     * 中文名称，当需要根据后台定义生成页面时，此字段为页面字段中文名
     */
    private String display;

    private String validator;

    private String size;

    /**
     * 是否为权限字段，权限字段会在增删改查所有数据操作时自动补入sql
     */
    private Boolean isAuthorityField;
    /**
     * 表单数据显示时是否隐藏，暂不作为现在列表数据的标识，列表字段手动选择确定，被选字段都应显示
     */
    private Boolean isHidden;
    /**
     * 组件类别
     */
    private String component;

    private List<PageFieldSaveDto> groupFields;

    private Map<String, String> options;
}
