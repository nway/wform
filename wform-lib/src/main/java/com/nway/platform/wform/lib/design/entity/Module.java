package com.nway.platform.wform.lib.design.entity;

import com.nway.spring.jdbc.annotation.Table;
import lombok.Data;

/**
 * 模块，相当于页面组，用于管理Page
 */
@Data
@Table("t_form_module")
public class Module extends BaseEntity {

    private String id;

    private String name;

    private String title;

    private String summary;

}
