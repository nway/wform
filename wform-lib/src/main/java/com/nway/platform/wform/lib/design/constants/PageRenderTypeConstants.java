package com.nway.platform.wform.lib.design.constants;

public interface PageRenderTypeConstants {
    int PAGE_RENDER_TYPE_PC = 1;
    int PAGE_RENDER_TYPE_ANDROID_PHONE = 2;
    int PAGE_RENDER_TYPE_ANDROID_PAD = 3;
    int PAGE_RENDER_TYPE_IOS_PHONE = 4;
    int PAGE_RENDER_TYPE_IOS_PAD = 5;
}
