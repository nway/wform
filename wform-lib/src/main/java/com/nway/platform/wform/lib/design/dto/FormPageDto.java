package com.nway.platform.wform.lib.design.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 表单页面
 */
@Data
public class FormPageDto extends BasePage {

    private Boolean isManual;

    /**
     * 新增数据时是否需要全局事务
     */
    private Boolean gtxCreate;
    /**
     * 修改数据时是否需要全局事务
     */
    private Boolean gtxUpdate;
    /**
     * 修改数据时是否需要全局事务
     */
    private Boolean gtxDelete;

    private Boolean editable;

}
