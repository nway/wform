package com.nway.platform.wform.lib.components.impl;

import com.nway.platform.wform.lib.components.SingleValueComponent;
import com.nway.platform.wform.lib.design.db.datatype.DatabaseDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Component("localDateTimeComponent")
public class LocalDateTimeComponent implements SingleValueComponent {

    @Autowired
    private DatabaseDialect databaseDialect;

    @Override
    public String getSqlType(String capacity) {
        return databaseDialect.getTimestamp();
    }

    @Override
    public Class<?> getJavaType() {
        return LocalDateTime.class;
    }

    @Override
    public Object getValue(Object value) {
        if (value == null || value.toString().length() == 0) {
            return value;
        }
        try {
            return LocalDateTime.parse((String) value, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        } catch (DateTimeParseException e) {
            return LocalDateTime.parse((String) value, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        }
    }
}
