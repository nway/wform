package com.nway.platform.wform.lib.design.dao;

import com.nway.platform.wform.lib.design.entity.FormField;
import com.nway.spring.jdbc.SqlExecutor;
import com.nway.spring.jdbc.sql.SQL;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository
public class FormFieldDao {

    @Autowired
    private SqlExecutor sqlExecutor;

    public List<FormField> listByPageId(String pageId) {
        return sqlExecutor.queryList(SQL.query(FormField.class).eq(FormField::getOwnerId, pageId));
    }

    public Map<String, FormField> mapByPageId(String pageId) {
        return listByPageId(pageId).stream().collect(Collectors.toMap(FormField::getId, Function.identity()));
    }

    public void insert(List<FormField> params) {
        sqlExecutor.batchInsert(params);
    }

    public void update(List<FormField> params) {
        sqlExecutor.batchUpdateById(params);
    }

    public void deleteById(List<String> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return;
        }
        sqlExecutor.batchDeleteById(ids, FormField.class);
    }
}
