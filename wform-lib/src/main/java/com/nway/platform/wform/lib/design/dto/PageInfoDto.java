package com.nway.platform.wform.lib.design.dto;

import com.nway.spring.jdbc.annotation.Column;
import com.nway.spring.jdbc.annotation.enums.ColumnType;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 不是指具体的页面，
 */
@Data
public class PageInfoDto {

    @Column(type = ColumnType.ID)
    private String id;

    private String name;

    private String title;

    private int status;

    private Boolean isManual;

    private Boolean gtxCreate;

    private Boolean gtxUpdate;

    private Boolean gtxDelete;

    private String tableName;

    private String moduleId;

    private String summary;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    @Override
    public String toString() {
        return "name=" + name + " tableName=" + tableName;
    }
}
