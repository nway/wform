package com.nway.platform.wform.lib.access;

public enum PageDataAccessTypeEnum {

    CREATE,

    UPDATE,

    DETAILS,

    LIST,

    REMOVE;

}