package com.nway.platform.wform.lib.access.handler;

import com.nway.platform.wform.lib.design.dto.BasePage;
import com.nway.platform.wform.lib.design.dto.FormPageDto;

import java.util.Map;

public interface PageDataHandler {

    /**
     * 操作数据时的前置处理，对数据的操作可以直接修改param
     *
     * @param handlerType
     * @param page
     * @param param
     */
    void handleParam(HandlerType handlerType, BasePage page, Map<String, Object> param);

    /**
     * 操作数据时的后置处理，对数据的操作可以直接修改data
     *
     * @param handlerType
     * @param page
     * @param data
     */
    void handleResult(HandlerType handlerType, BasePage page, Object data);
}
