package com.nway.platform.wform.lib.design.dao;

import com.nway.platform.wform.lib.design.entity.ListField;
import com.nway.spring.jdbc.SqlExecutor;
import com.nway.spring.jdbc.sql.SQL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ListPageDao {

    @Autowired
    private SqlExecutor sqlExecutor;

    public List<ListField> listByPageId(String pageId) {
        return sqlExecutor.queryList(SQL.query(ListField.class).eq(ListField::getPageId, pageId).orderBy(ListField::getOrderNum));
    }

    public void insert(List<ListField> params) {
        sqlExecutor.batchInsert(params);
    }
}
