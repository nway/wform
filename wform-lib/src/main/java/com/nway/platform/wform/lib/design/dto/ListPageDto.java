package com.nway.platform.wform.lib.design.dto;

import lombok.Data;

import java.util.List;

/**
 * 页面列表
 */
@Data
public class ListPageDto extends BasePage {

    private List<ListPageFieldDto> conditionField;

    private List<ListPageFieldDto> orderField;
}
