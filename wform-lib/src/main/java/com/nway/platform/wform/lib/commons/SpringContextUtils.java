package com.nway.platform.wform.lib.commons;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.stereotype.Component;

/**
 * 获取spring容器，以访问容器中定义的其他bean
 */
@Component
public class SpringContextUtils implements ApplicationContextAware {
	
	// Spring应用上下文环境
	private static ApplicationContext applicationContext;

	/**
	 * 实现ApplicationContextAware接口的回调方法，设置上下文环境
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		SpringContextUtils.applicationContext = applicationContext;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * 获取对象 这里重写了bean方法，起主要作用
	 * 
	 * @param name
	 * @return Object 一个以所给名字注册的bean的实例
	 * @throws BeansException
	 */
	public static Object getBean(String name) {
		
		return applicationContext.getBean(name);
	}

	public static <T> T getBean(String name, Class<T> requiredType) {
		
		return applicationContext.getBean(name, requiredType);
	}
	
	public static <T> T getBean(String name, Class<T> requiredType, T defaultObj) {

		if (applicationContext.containsBean(name)) {

			return applicationContext.getBean(name, requiredType);
		}

		return defaultObj;
	}
	
	public static <T> T getBean(Class<T> requiredType) {
		
		return applicationContext.getBean(requiredType);
	}
	
	public static <T> Map<String, T> getBeansOfType(Class<T> type) {

		return applicationContext.getBeansOfType(type, true, false);
	}
	
	public static void publishEvent(ApplicationEvent event) {
		
		applicationContext.publishEvent(event);
	}
}