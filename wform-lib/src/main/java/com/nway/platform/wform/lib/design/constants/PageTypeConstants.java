package com.nway.platform.wform.lib.design.constants;

public interface PageTypeConstants {

    String PAGE_TYPE_FORM = "form";
    String PAGE_TYPE_LIST = "list";
    String PAGE_TYPE_NESTED = "nested";
}
