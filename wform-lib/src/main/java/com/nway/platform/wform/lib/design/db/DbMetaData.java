package com.nway.platform.wform.lib.design.db;

import com.nway.platform.wform.lib.commons.SpringContextUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@DependsOn("springContextUtils")
public class DbMetaData implements InitializingBean {

	private String databaseProductName;

	private static final Pattern DATABASE_NAME_PATTERN = Pattern.compile("(\\w+)");

	public String getDatabaseProductName() {
		
		return databaseProductName;
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {

		DataSource ds = SpringContextUtils.getBean(DataSource.class);
		Connection conn = DataSourceUtils.getConnection(ds);

		try {
			String databaseProductName = conn.getMetaData().getDatabaseProductName();
			Matcher matcher = DATABASE_NAME_PATTERN.matcher(databaseProductName);
			if(matcher.find()) {
				this.databaseProductName = matcher.group(1);
			}
		}
		catch(BeansException | SQLException e) {
			throw e;
		} finally {
			DataSourceUtils.doCloseConnection(conn, ds);
		}
	}

	
}
