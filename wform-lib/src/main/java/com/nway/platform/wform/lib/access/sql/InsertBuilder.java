package com.nway.platform.wform.lib.access.sql;

import com.nway.spring.jdbc.sql.SqlType;
import com.nway.spring.jdbc.sql.builder.SqlBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InsertBuilder extends SqlBuilder<InsertBuilder> {

    private final List<String> columns = new ArrayList<>();
    private final StringBuilder sql = new StringBuilder();
    private final List<Object> param = new ArrayList<>();
    private final String tableName;

    public InsertBuilder(Class<?> beanClass, String tableName) {
        super(beanClass);
        this.tableName = tableName;
    }

    public InsertBuilder use(Map<String, Object> data) {
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            if (entry.getValue() != null && !isInvalid(entry.getValue())) {
                columns.add(entry.getKey());
                param.add(entry.getValue());
            }
        }
        return this;
    }

    @Override
    protected SqlType getSqlType() {
        return SqlType.INSERT;
    }

    @Override
    public String getSql() {
        sql.append("insert into ")
                .append(tableName).append(" (")
                .append(String.join(",", columns))
                .append(") values (")
                .append(columns.stream().map(e -> "?").collect(Collectors.joining(",")))
                .append(")");
        return sql.toString();
    }

    @Override
    public List<Object> getParam() {
        return this.param;
    }
}
