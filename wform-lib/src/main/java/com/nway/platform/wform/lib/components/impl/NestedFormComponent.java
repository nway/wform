package com.nway.platform.wform.lib.components.impl;

import com.google.common.collect.Maps;
import com.nway.platform.wform.lib.access.PageDataService;
import com.nway.platform.wform.lib.components.FieldGroupComponent;
import com.nway.platform.wform.lib.components.SingleValueComponent;
import com.nway.platform.wform.lib.design.dto.BasePage;
import com.nway.platform.wform.lib.design.dto.FormPageDto;
import com.nway.platform.wform.lib.design.dto.PageFieldDto;
import com.nway.platform.wform.lib.design.dto.PageFieldGroupDto;
import com.nway.platform.wform.lib.design.service.PageService;
import com.nway.spring.jdbc.SqlExecutor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Log4j2
@Component("nestedFormComponent")
public class NestedFormComponent implements FieldGroupComponent {

    @Autowired
    private SqlExecutor sqlExecutor;
    @Autowired
    private PageService pageService;
    @Autowired
    private PageDataService pageDataService;

    /**
     * 
     * @param field 关联字段
     * @param val   同 {@link #getValue(PageFieldDto, Object)}
     * @return
     */
    @Override
    public Object getParentId(PageFieldDto field, Object val) {

        BasePage formPage = pageService.getById(field.getOwnerId());

        return ((Map) val).get(formPage.getKeyField().getNameCamel());
    }

    @Override
    public Object getValue(PageFieldDto field, Object val) {

        if (!(field instanceof PageFieldGroupDto)) {
            log.warn("字段类型不正确，期望是字段组");
            return val;
        }

        BasePage formPage = pageService.getById(field.getId());

        Map<String, Object> retVal = new HashMap<>();
        for (PageFieldDto fieldDto : formPage.getFields()) {
            if (!fieldDto.getIsMultiValue()) {
                retVal.put(fieldDto.getNameCamel(), ((SingleValueComponent) fieldDto.getComponentObj()).getValue(retVal.get(fieldDto.getName())));
            }
        }

        return retVal;
    }

    @Override
    public void create(PageFieldDto field, String bizId, Object comp) {

        if (!(field instanceof PageFieldGroupDto)) {
            log.warn("字段类型不正确，期望是字段组");
            return;
        }

        BasePage formPage = pageService.getById(field.getId());

        Map<String, Object> pageData = (Map) comp;
        pageData.put(formPage.getKeyField().getNameCamel(), bizId);
        pageData.put("WFORM_FORCE_INSERT_INNER", true);

        pageDataService.save(formPage.getName(), pageData);
    }

    @Override
    public void update(PageFieldDto field, String bizId, Object comp) {

        if (!(field instanceof PageFieldGroupDto)) {
            log.warn("字段类型不正确，期望是字段组");
            return;
        }

        String nestedPageName = pageService.getById(field.getId()).getName();

        pageDataService.save(nestedPageName, (Map) comp);
    }

    @Override
    public void remove(PageFieldDto field, List<String> bizIds) {

        FormPageDto formPage = (FormPageDto) pageService.getById(field.getOwnerId());

        String nestedPageName = pageService.getFieldMeta(field.getOwnerId(), field.getName(), "nested_page_name");
        String relationFieldName = pageService.getFieldMeta(field.getOwnerId(), field.getName(), "relation_field_name");
        relationFieldName = Optional.ofNullable(relationFieldName).orElse(formPage.getKeyField().getName());

        String sql = "select " + formPage.getKeyField().getName() + " from "
                + formPage.getTableName() + " where "
                + relationFieldName + " in (" + IntStream.range(0, bizIds.size()).mapToObj(e -> "?").collect(Collectors.joining(",")) + ")";

        List<String> keys = sqlExecutor.queryList(sql, String.class, bizIds.toArray());

        pageDataService.remove(nestedPageName, keys);
    }

    /**
     * 此方法查询所有字段 包括多值字段  可能会造成不必要的性能消耗
     *
     * @param field
     * @param bizId
     * @return
     */
    @Override
    public Map<String, Object> getForForm(PageFieldDto field, String bizId) {
        String nestedPageName = pageService.getFieldMeta(field.getOwnerId(), field.getName(), "nested_page_name");
        return pageDataService.get(nestedPageName, bizId);
    }

    /**
     * 实用中  此方法应该被重写
     *
     * @param field
     * @param bizIdList
     * @return
     */
    @Override
    public Map<String, Map<String, Object>> getForList(PageFieldDto field, List<String> bizIdList) {
        if (CollectionUtils.isEmpty(bizIdList)) {
            return Maps.newHashMap();
        }
        String nestedPageName = pageService.getFieldMeta(field.getOwnerId(), field.getName(), "nested_page_name");
        BasePage formPage = pageService.getByName(nestedPageName);
        String keyFileName = formPage.getKeyField().getNameCamel();
        Map<String, Object> param = new HashMap<>();
        param.put(keyFileName, bizIdList);
        return pageDataService.queryList(formPage.getName(), param).stream().collect(Collectors.toMap(e -> e.get(keyFileName).toString(), Function.identity()));
    }

    @Override
    public void onPublish(PageFieldDto field) {
    }

    @Override
    public Class<?> getJavaType() {
        return Map.class;
    }
}
