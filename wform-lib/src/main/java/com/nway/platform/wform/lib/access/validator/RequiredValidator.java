package com.nway.platform.wform.lib.access.validator;

import org.springframework.stereotype.Component;

@Component("requiredValidator")
public class RequiredValidator implements Validator {

    @Override
    public String validate(String fieldName, Object fieldData) {
        if (fieldData == null || fieldData.toString().trim().length() == 0) {
            return "请为字段" + fieldName + "填写内容";
        }
        return null;
    }
}
