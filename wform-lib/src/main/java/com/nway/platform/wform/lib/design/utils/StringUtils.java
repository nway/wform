package com.nway.platform.wform.lib.design.utils;

public class StringUtils extends org.apache.commons.lang3.StringUtils {

    public static String convertToLowCamel(String fieldName) {
        StringBuilder str = new StringBuilder();
        char[] chars = fieldName.toCharArray();
        int underLineIdx = -1;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '_') {
                underLineIdx = i + 1;
            } else if (i == underLineIdx) {
                str.append(String.valueOf(chars[i]).toUpperCase());
            } else {
                str.append(chars[i]);
            }
        }
        return str.toString();
    }

    public static String convertToLowUnder(String fieldName) {
        StringBuilder str = new StringBuilder();
        char[] chars = fieldName.toCharArray();
        for (char aChar : chars) {
            if (Character.isUpperCase(aChar)) {
                str.append('_').append(String.valueOf(aChar).toLowerCase());
            } else {
                str.append(aChar);
            }
        }
        return str.toString();
    }

    public static String blankToDefault(String src, String dfut) {
        if (src == null || src.trim().length() == 0) {
            return dfut;
        }
        return src;
    }
}
