package com.nway.platform.wform.lib.components.impl;

import com.nway.platform.wform.lib.components.SingleValueComponent;
import com.nway.platform.wform.lib.design.db.datatype.DatabaseDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nway.platform.wform.lib.components.BaseComponent;

@Component("textComponent")
public class TextComponent implements SingleValueComponent {

    @Autowired
    private DatabaseDialect databaseDialect;

    @Override
    public Object getValue(Object value) {

        return value;
    }

    @Override
    public String getSqlType(String capacity) {

        return databaseDialect.getString(capacity);
    }

    @Override
    public Class<?> getJavaType() {
        return String.class;
    }
}
