package com.nway.platform.wform.lib.design.dto;

import lombok.Data;

/**
 * 页面字段的定义，与页面无关的基础信息
 */
@Data
public class ListPageFieldDto extends PageFieldDto {

    private String searchType;

    private String orderType;

    @Override
    public String toString() {
        return "name=" + getName() + " component=" + getComponent();
    }
}
