package com.nway.platform.wform.lib.access.handler;

public enum HandlerType {

	DATA_CREATE, DATA_LIST, DATA_MODIFY, DATA_DETAILS, DATA_REMOVE;
}
