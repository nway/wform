package com.nway.platform.wform.lib.design.dao;

import com.nway.platform.wform.lib.design.entity.FormFieldAttr;
import com.nway.spring.jdbc.SqlExecutor;
import com.nway.spring.jdbc.sql.SQL;
import com.nway.spring.jdbc.sql.builder.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class FormFieldAttrDao {

    @Autowired
    private SqlExecutor sqlExecutor;

    public void delete(List<String> fieldIds) {
        sqlExecutor.delete(SQL.delete(FormFieldAttr.class).in(FormFieldAttr::getFieldId, fieldIds));
    }

    public List<FormFieldAttr> listByFieldId(List<String> fieldIds) {
        return sqlExecutor.queryList(SQL.query(FormFieldAttr.class).in(FormFieldAttr::getFieldId, fieldIds));
    }

    public Map<String, String> getVal(List<String> fieldIds, String attrName) {
        Map<String, String> retVal = new HashMap<>();
        QueryBuilder queryBuilder = SQL.query(FormFieldAttr.class).in(FormFieldAttr::getFieldId, fieldIds).eq(FormFieldAttr::getAttrName, attrName);
        List<FormFieldAttr> attrList = sqlExecutor.queryList(queryBuilder);
        for (FormFieldAttr fieldAttr : attrList) {
            retVal.put(fieldAttr.getFieldId(), fieldAttr.getAttrValue());
        }
        return retVal;
    }

    public void insert(List<FormFieldAttr> params) {
        sqlExecutor.batchInsert(params);
    }
}
