package com.nway.platform.wform.lib.components.impl;

import com.nway.platform.wform.lib.components.SingleValueComponent;
import com.nway.platform.wform.lib.design.db.datatype.DatabaseDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("integerComponent")
public class IntegerComponent implements SingleValueComponent {

    @Autowired
    private DatabaseDialect databaseDialect;

    @Override
    public String getSqlType(String capacity) {
        return databaseDialect.getInteger(capacity);
    }

    @Override
    public Object getValue(Object value) {
        return value == null ? null : Integer.valueOf(value.toString());
    }

    @Override
    public Class<?> getJavaType() {
        return Integer.class;
    }
}
