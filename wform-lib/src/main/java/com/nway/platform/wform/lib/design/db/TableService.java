package com.nway.platform.wform.lib.design.db;

import com.nway.platform.wform.lib.components.BaseComponent;
import com.nway.platform.wform.lib.components.ComponentRegister;
import com.nway.platform.wform.lib.components.MultiValueComponent;
import com.nway.platform.wform.lib.components.SingleValueComponent;
import com.nway.platform.wform.lib.components.impl.KeyComponent;
import com.nway.platform.wform.lib.design.entity.FormField;
import com.nway.platform.wform.lib.design.utils.StringUtils;
import com.nway.spring.jdbc.SqlExecutor;
import com.nway.spring.jdbc.sql.SQL;
import com.nway.spring.jdbc.sql.builder.QueryBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TableService {

    @Autowired
    private SqlExecutor sqlExecutor;
    @Autowired
    private ComponentRegister componentRegister;

    public void alterColumn(String tableName, List<FormField> insertList, List<FormField> updateList) {

        StringBuilder sql = new StringBuilder();
        sql.append("ALTER TABLE ").append(tableName).append(" ");
        int initLen = sql.length();

        // 新增的
        for (FormField formField : insertList) {
            BaseComponent component = componentRegister.getComponent(formField.getComponent());
            if (component instanceof MultiValueComponent || component instanceof KeyComponent) {
                continue;
            }
            String lowUnder = StringUtils.convertToLowUnder(formField.getName());
            sql.append("ADD COLUMN ").append(lowUnder).append(" ")
                    .append(((SingleValueComponent) component).getSqlType(formField.getSize()))
                    .append(" COMMENT '").append(formField.getDisplay()).append("',");
        }

        // 修改的
        for (FormField formField : updateList) {
            BaseComponent component = componentRegister.getComponent(formField.getComponent());
            if (component instanceof MultiValueComponent || component instanceof KeyComponent) {
                continue;
            }
            String lowUnder = StringUtils.convertToLowUnder(formField.getName());
            String lowUnderOld = StringUtils.convertToLowUnder(formField.getOldName());
            sql.append("CHANGE COLUMN ").append(lowUnderOld).append(" ").append(lowUnder).append(" ")
                    .append(((SingleValueComponent) component).getSqlType(formField.getSize()))
                    .append(" COMMENT '").append(formField.getDisplay()).append("',");
        }

        if (sql.length() > initLen) {
            String sqlExec = sql.deleteCharAt(sql.length() - 1).toString();
            log.debug("修改表结构 : {}", sqlExec);
            sqlExecutor.getJdbcTemplate().execute(sqlExec);
        }

    }

    private Map<String, TableColumn> listColumnMap(String tableName) {
        QueryBuilder queryBuilder = SQL.query(TableColumn.class)
                .eq(TableColumn::getTableName, tableName);
        return sqlExecutor.<TableColumn>queryList(queryBuilder).stream().collect(Collectors.toMap(TableColumn::getColumnName, Function.identity()));
    }

}
