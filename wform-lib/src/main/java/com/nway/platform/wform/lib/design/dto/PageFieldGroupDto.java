package com.nway.platform.wform.lib.design.dto;

import lombok.Data;

import java.util.List;

@Data
public class PageFieldGroupDto extends PageFieldDto {

    private String tableName;

    private List<PageFieldDto> fields;

}
