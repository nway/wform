package com.nway.platform.wform.lib.design.query;

import lombok.Data;

@Data
public class PageParam {

    private int pageNum;

    private int pageSize;
}
