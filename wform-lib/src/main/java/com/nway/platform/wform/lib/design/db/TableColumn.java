package com.nway.platform.wform.lib.design.db;

import com.nway.spring.jdbc.annotation.Table;
import lombok.Data;

@Data
@Table("COLUMNS")
public class TableColumn {

    private String tableName;

    private String columnName;

    private String columnType;

    private String columnComment;
}
