package com.nway.platform.wform.lib.design.dto;

import com.nway.platform.wform.lib.design.constants.PageTypeConstants;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class BasePage {

    private String id;

    private String parentId;

    private String name;

    private String title;

    /**
     * 表单页面、列表页面
     * <p>
     * 详见 {@link PageTypeConstants }
     */
    private String type;

    private String tableName;

    private String moduleId;

    private String moduleName;
    /**
     * 简介
     */
    private String summary;

    private LocalDateTime createTime;

    private int status;

    private PageFieldDto keyField;

    private List<PageFieldDto> fields;

    @Override
    public String toString() {
        return "name=" + name + " tableName=" + tableName;
    }
}
