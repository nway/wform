package com.nway.platform.wform.lib.design.db.datatype;

public interface DatabaseDialect {

    String getInteger(String capacity);

    String getString(String capacity);

    String getFloat(String capacity);

    String getDouble(String capacity);

    String getDecimal(String capacity);

    String getBoolean();

    String getDate();

    String getTimestamp();
}
