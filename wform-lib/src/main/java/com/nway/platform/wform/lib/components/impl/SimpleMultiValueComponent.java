package com.nway.platform.wform.lib.components.impl;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.nway.platform.wform.lib.components.Initializable;
import com.nway.platform.wform.lib.components.MultiValueComponent;
import com.nway.platform.wform.lib.components.impl.dao.CheckboxDao;
import com.nway.platform.wform.lib.design.dto.BasePage;
import com.nway.platform.wform.lib.design.dto.PageFieldDto;
import com.nway.platform.wform.lib.design.dto.PageInfoDto;
import com.nway.platform.wform.lib.design.dto.FormPageDto;
import com.nway.platform.wform.lib.design.service.PageService;
import com.nway.spring.jdbc.SqlExecutor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component("checkboxComponent")
public class SimpleMultiValueComponent implements MultiValueComponent, Initializable {

    @Autowired
    private CheckboxDao checkboxDao;
    @Autowired
    private PageService pageService;
    @Autowired
    private SqlExecutor sqlExecutor;

    @Override
    public Class<?> getJavaType() {
        return List.class;
    }

    @Override
    public Object getParentId(PageFieldDto field, Object val) {
        if (val == null || ((String) val).length() == 0) {
            return null;
        }
        return field.getOwnerId();
    }

    @Override
    public List<Object> getValue(PageFieldDto field, Object val) {
        if (val == null) {
            return null;
        }
        BasePage page = pageService.getById(field.getOwnerId());
        String tableName = makeTableName(page.getTableName(), field.getNameUnder());
        return checkboxDao.listBizId(tableName, JSON.parseArray(val.toString(), Object.class));
    }

    @Override
    public void create(PageFieldDto field, String bizId, Object compValue) {

        if (compValue == null) {
            return;
        }

        BasePage page = pageService.getById(field.getOwnerId());
        String tableName = makeTableName(page.getTableName(), field.getNameUnder());

        checkboxDao.clear(tableName, Lists.newArrayList(bizId));
        checkboxDao.save(tableName, bizId, compValue);
    }

    @Override
    public void update(PageFieldDto field, String bizId, Object compValue) {

        create(field, bizId, compValue);
    }

    @Override
    public void remove(PageFieldDto field, List<String> bizId) {

        BasePage page = pageService.getById(field.getOwnerId());

        String tableName = makeTableName(page.getTableName(), field.getNameUnder());

        checkboxDao.clear(tableName, bizId);
    }

    @Override
    public Map<String, Object> getForForm(PageFieldDto field, String bizId) {

        if (StringUtils.isBlank(bizId)) {
            return Maps.newHashMap();
        }

        Map<String, Object> retVal = new HashMap<>();

        BasePage page = pageService.getById(field.getOwnerId());

        String tableName = makeTableName(page.getTableName(), field.getNameUnder());

        List<Map<String, Object>> values = checkboxDao.getValues(tableName, Lists.newArrayList(bizId));

        Map<String, String> init = (Map<String, String>) init(field);
        Map<Object, Object> data = new HashMap<>();
        for (Map<String, Object> value : values) {
            Object val = value.getOrDefault("value", "");
            data.put(val, init.getOrDefault(val, "未知数据"));
        }
        retVal.put(field.getNameCamel(), data);
        return retVal;
    }

    @Override
    public Map<String, Map<String, Object>> getForList(PageFieldDto field, List<String> bizIdList) {
        return null;
    }

    @Override
    public Object init(PageFieldDto field) {

        Map<String, String> map = new HashMap<>();

        map.put("1001", "Java");
        map.put("1002", "C++");

        return map;
    }

    @Override
    public void onPublish(PageFieldDto field) {

        StringBuilder sql = new StringBuilder();

        PageInfoDto page = pageService.getBasePage(field.getOwnerId());

        sql.append("create table ").append(makeTableName(page.getTableName(), field.getNameUnder())).append("(id varchar(36) primary key,biz_id varchar(36),value varchar(32))");

        sqlExecutor.getJdbcTemplate().execute(sql.toString());
    }

    private String makeTableName(String pageTable, String fieldName) {
        return pageTable + "_" + fieldName;
    }
}
